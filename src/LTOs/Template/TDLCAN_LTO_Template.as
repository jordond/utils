/**
 * @description
 * A template class to use for building new transparent LTOs.
 *
 * Add your MovieClips to the FLA, and treat them like a promo.  Prices will be price1, price2, etc.
 * To customize the LTO edit the function "determineActiveLTO".
 * To publish the LTO, you can use the promo publish script found on the server
 * \\qnap03.ek3.com\LDNClients\Active\WIS\TDL\Promos\_Template_\publish.jsfl
 *
 * TODO - Better handle the way calories are added to the lunch ltos
 * TODO - Update the logging for the bottle bev check, disable showing nodes dont need, add "should see x products"
 * TODO - Implement some changes from PROMO template
 * TODO - Output which nodes are being checked for reducing lto
 * TODO - Better handle the blank LTO on a cold bev reduced board.  currently throws an error
 *
 **/
package {

	import com.ek3.dmb.debug.Debugger;
	import com.ek3.dmb.display.DisplayModule;
	import com.ek3.dmb.display.playlists.BasicPlaylist;
	import com.ek3.dmb.external.data.DataStorage;
	import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
	import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
	import com.ek3.dmb.platforms.MediaPlatform;
	import com.ek3.dmb.settings.AppSettings;

	import flash.events.Event;
	import flash.text.TextField;
	import flash.utils.setTimeout;

	public class TDLCAN_LTO_Template extends MediaPlatform {
		private const APP_NAME:String = "TDLCAN_LTO_Template";
		private const DATA_TIMEOUT:Number = 10 * 1000; // 10 Seconds

		private var versionNumber:Number = 1;
		private var debugLevel:Number = 1;
		private var menuboardObj:MenuboardV2DataFeed;
		private var dataStorage:DataStorage;
		
		private var mainModule:LTOModule;
		private var boardSettings:Object = { debug: false };
		private var debugTxtMC:TextField = getDebugText();

		public function TDLCAN_LTO_Template():void {
			AppSettings.treatPriorityAsDisplay = true;
			this.language = "Any";
			this.appName = APP_NAME;
			this.debug(debugLevel);
			this.playLength = 10;

			const debugger = Debugger.getInstance();
			if (debugger.hasOwnProperty("toggleDebugCallback")) { // Check to make sure function exists before calling, keep until PR is merged
				Debugger.getInstance()["toggleDebugCallback"] = toggleDebugMode;
			} else {
				trace("WARN: Toggle Debug callback does not exist in the Library");
			}

			// Wrap in try-catch so exception can be shown in debug mode
			try {
				//Set up Data Storage, Listeners and vars
				dataStorage = DataStorage.getInstance();
				dataStorage.addEventListener(DataStorage.ALL_DATA_RECEIVED, onDataReady);
				var requiredData:Array = new Array();

				//Create MBV2 DataFeed
				menuboardObj = new MenuboardV2DataFeed(30*1000);
				requiredData.push(menuboardObj);

				// Try to parse Misc settings passed in through the FLA
				var cleanJSON = CONFIG::SETTINGS.replace(/([$])([$])?/g, '"') || '{}';
				boardSettings = JSON.parse(cleanJSON);
				boardSettings.language = CONFIG::LANGUAGE.toUpperCase();
				boardSettings.board = CONFIG::BOARD.toUpperCase();
				showDebugMessage('Board Settings', boardSettings);
				boardSettings.debugTxtMC = debugTxtMC;

				//Create Display Modules
				mainModule = new LTOModule(boardSettings);
				mainModule.addDataRequirement(menuboardObj.name);
				this.displayArea.addChild(mainModule);

				//Start Loading
				dataStorage.fetchData(requiredData);
				setTimeout(function () {
					if (!boardSettings.dataLoaded) {
						showDebugMessage("Data was not loaded within timeout period, make sure a 'menuboardv2.xml' exists");
					}
				}, DATA_TIMEOUT);
			} catch (ex) {
				trace("ERROR: During settings parse, or board creation");
				showDebugMessage(ex);
				throw ex;
			}
		}

		private function getDebugText():TextField {
			if (!debugTxtMC) {
				var debugText = new TextField();
				debugText.background = true;
				debugText.backgroundColor = 0x000000;
				debugText.textColor = 0xFFFFFF;
				debugText.height = stage.stageHeight;
				debugText.width = stage.stageWidth;
				debugText.alpha = 0.6;
				debugTxtMC = debugText
			}
			return debugTxtMC;
		}

		private function showDebugMessage(message:String, data:Object = null) {
			if (data) { message += '\n' + JSON.stringify(data, null, 2) };
			if (debugTxtMC) {
				debugTxtMC.appendText(message + "\n");
				debugTxtMC.visible = boardSettings.debug;
			}
		}

		private function toggleDebugMode(enabled:Boolean):void {
			boardSettings.debug = enabled;
			if (boardSettings.debugTxtMC) {
				addChild(boardSettings.debugTxtMC);
			}
			showDebugMessage("Debug mode: " + boardSettings.debug);
		}

		private function onDataReady(e:Event = null):void {
			boardSettings.dataLoaded = true;
			mainModule.build();
		}
	}
}

import com.ek3.dmb.display.DisplayModule;
import com.ek3.dmb.display.DynamicPromo
import com.ek3.dmb.external.data.DataStorage;
import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
import com.ek3.dmb.external.data.menuboard.MenuboardProduct;
import com.ek3.dmb.external.data.menuboard.MenuboardProductList;
import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
import com.ek3.dmb.settings.namingconvention.DisplayNames;
import com.ek3.dmb.utils.DisplayUtils;

import flash.display.MovieClip;

internal class LTOModule extends DisplayModule {
	private const BASE_GENERIC_NAME = "Generic_";
	private const REDUCED_SUFFIX = "_Reduced";
	private const GENERIC_LTO_NODE_BASE = "lto_generic_";

	private const LANGUAGE_EN = "EN";
	private const LANGUAGE_FR = "FR";
	private const LANGUAGE_BI = "BI";
	private const LANGUAGE_DEFAULT = LANGUAGE_EN;
	
	private const BOARD_HOT = "HOT";	 // Display the HOT generic, and hide when calories are enabled
	private const BOARD_COLD = "COLD";	 // Display the COLD generic, and hide when calories are enabled
	private const BOARD_LUNCH = "LUNCH"; // Display the LUNCH generic, show calorie version of LTOs
	private const BOARD_NONE = "NONE";	 // Don't use a generic at all, show transparent instead

	/**
	 * For HOT and COLD boards, if ANY of these certain nodes are ON the LTO must be transparent
	 * Add nodes to DISABLE_HOTCOLD to affect both, and DISABLE_EXTRA_HOT/DISABLE_EXTRA_COLD for specific boards
	 */
	private const DISABLE_HOTCOLD = ["calories", "espresso_test_vancouver", "espresso_test_london"];
	private const DISABLE_EXTRA_HOT = ["espresso_rollout_bc"];
	private const DISABLE_EXTRA_COLD = [];

	private var _dataFeed:MenuboardV2DataFeed;
	private var _boardSettings:Object;

	/** ======================================================================
	 * All LTO selection logic will go in here
	 * Generic LTO and the reducing of the LTO is handled automatically, based on the _boardSettings.board variable
	 * Ensure lto linkages are suffexed with language tag ex: AwesomeLTO_EN, AwesomeLTO_FR
	 */
	private function determineActiveLTO():DynamicPromo {
		// Main LTO
		var ltoLinkageName:String = "Template";

		var replace2:MenuboardFeedItem = getItem("node");
		var replace3:MenuboardFeedItem = getItem("node");

		var replaceIsAvailable:Boolean = multipleAreAvailable(["lto_node_goes_here", replace2, replace3], ltoLinkageName);

		if (replaceIsAvailable) {
			return createLTOInstance(ltoLinkageName, [replace2.price, replace3.price]);
		}

		// Add additional LTO checks here

		// If no LTO's are available, show a generic LTO based on the current board (_boardSettings.board)
		// Note: You can override by passing specific generic constant ie: displayGeneric(BOARD_LUNCH)
		// Setting to BOARD_NONE will hide the generic all together (completely transparent)
		return displayGeneric();

		/** End customization **/
	}
	/**
	 * End of LTO selection logic
	 * ======================================================================
	 */
	
	public function LTOModule(boardSettings:Object):void {
		boardSettings.language = validateSetting([LANGUAGE_EN, LANGUAGE_FR, LANGUAGE_BI], boardSettings.language, LANGUAGE_DEFAULT)
		boardSettings.board = validateSetting([BOARD_HOT, BOARD_COLD, BOARD_LUNCH, BOARD_NONE], boardSettings.board, BOARD_NONE)
		_boardSettings = boardSettings;

		// Currently BEF5050 LTOs are the same as EN
		if (_boardSettings.overrideLang && _boardSettings.overrideLang is String) {
			log("INFO: overrideLang flag was set to -> " + _boardSettings.overrideLang);
			log("INFO: Bypassing normal language checks, ensure the overrideLang is set correctly, and the required MovieClips exist");
			_boardSettings.language = _boardSettings.overrideLang;
		} else {
			if (_boardSettings.language === LANGUAGE_BI) {
				_boardSettings.language = LANGUAGE_EN;
				log("INFO: BI language detected, overriding to " + LANGUAGE_EN + " instead");
			}
		}

		if (_boardSettings.board === BOARD_NONE) {
			log("INFO: Board mode is set to \"" + BOARD_NONE + "\", all board specific functionality will be disabled");
		}
		log("LTO Settings: \n	Language -> " + _boardSettings.language + "\n	Type -> " + _boardSettings.board);
	}


	private function displayGeneric(board:String = null):DynamicPromo {
		if (!board) {
			board = _boardSettings.board;
		}
		board = board.toUpperCase();

		// Don't show a generic if board is set to NONE, or the generic lto is disabled in BK
		const boardSpecificGenericNode:String = GENERIC_LTO_NODE_BASE + board.toLowerCase();
		const genericDisabled:Boolean = !checkAvailabilityAndTrace(boardSpecificGenericNode, false);
		if (board === BOARD_NONE || genericDisabled) {
			if (genericDisabled && board !== BOARD_NONE) {
				log("INFO: The generic node \"" + boardSpecificGenericNode + "\" is disabled, hiding the generic");
			}
			return null;
		}
		log("Board specific generic node [" + boardSpecificGenericNode + "] is available");

		const genericName:String = BASE_GENERIC_NAME + board;
		const genericLTO:DynamicPromo = createLTOInstance(genericName);

		// If a lunch LTO check to which frame to set the generic to
		if (board === BOARD_LUNCH) {
			const wedges:Boolean = checkAvailabilityAndTrace("potato_wedges");
			const water:Boolean = checkAvailabilityAndTrace("bottled_beverages_spring_water");
			var gotoFrame:String = "genericMsg";
			if (wedges && water){
				gotoFrame = "WedgesWater";
			} else if (water && !wedges){
				gotoFrame = "DonutWater";
			}
			if (DisplayUtils.movieClipHasLabel(genericLTO, gotoFrame)) {
				genericLTO.gotoAndStop(gotoFrame);
				log("Changing Lunch generic to the [" + gotoFrame + "] frame");
			}
		}
		return genericLTO;
	}

	override protected function doBuild():void {
		_dataFeed = DataStorage.getInstance().data["menuboardv2"];
		_boardSettings.calories = checkAvailabilityAndTrace("calories", false);
		if (_boardSettings.calories) {
			log("Calorie mode is enabled");
		}

		// Create the layout and attach it
		const boardContainer:MovieClip = new LTOLayout();
		addChild(boardContainer);

		// Capture any errors and print to the screen, then rethrow
		try {
			// Check to see if the LTO should display
			var activeLTO:DynamicPromo;
			if (shouldDisplayLTO(_boardSettings.board)) {
				log("Green light was given! Time to display something");
				// Determine which LTO to display, or which generic if LTO isn't available
				activeLTO = determineActiveLTO();
			}
			// If no active lto exists, or a generic could not be created display a blank LTO
			if (!activeLTO) {
				log("Unable to display an LTO or a generic, see the log");
				// Remove because it was causing a update on a data change
				// _boardSettings.board = BOARD_NONE; // TODO create better solution for blankLTO on a cold bev reduced board
				activeLTO = createLTOInstance("BlankLTO", null, null, true);
			}
			boardContainer.ltoHolder.addChild(activeLTO);
		} catch (err) {
			log(err.message, err.getStackTrace());
			throw err;
		}
	}
	
	/* Helper functions */

	// Trace to console, as well as debug text field if applicable
	private function log(message:String, data:Object = null) {
		var dataString:String = "";
		if (data) dataString = "\n" + JSON.stringify(data, null, 2);
		_boardSettings.debugTxtMC.appendText(message + "\n");
		trace(message);
	}
	
	// Validate a passed in setting with a list of options
	private function validateSetting(validItems:Array, setting:String, defaultSetting:String):String {
		if (validItems.indexOf(setting) === -1) {
			log("WARNING: Invalid setting [" + setting + "] was passed in defaulting to -> " + defaultSetting);
			return defaultSetting;
		}
		return setting;
	}

	// Check to see if the lto should display
	private function shouldDisplayLTO(boardType:String):Boolean {
		// COLD or HOT sometimes can't show an LTO
		if ([BOARD_HOT, BOARD_COLD].indexOf(boardType) !== -1) {
			log("Hot or Cold board detected, checking extra nodes to ensure LTO should display");
			const nodesToCheck = DISABLE_HOTCOLD;
			nodesToCheck.push.apply(nodesToCheck, (boardType === BOARD_HOT) ? DISABLE_EXTRA_HOT : DISABLE_EXTRA_COLD);

			const anyAreAvailable = nodesToCheck.some(checkAvailabilityCallback);
			if (anyAreAvailable) {
				log("One or more of the additional nodes are enabled, hiding the LTO");
				return false;
			}
		}
		return true;
	}

	private function getItem(serverName:String):MenuboardFeedItem {
		return _dataFeed.returnSpecificItem({ server_name: serverName });
	}

	// Check if multiple products are available
	private function multipleAreAvailable(items:Array, name:String = null): Boolean {
		if (name) {
			log(name + " Nodes");
		}
		const allItemsAvailable:Boolean = items.filter(checkAvailabilityCallback).length === items.length;
		log("	-> Available: " + (allItemsAvailable ? "yes" : "no"));
		return allItemsAvailable;
	}

	private function checkAvailabilityCallback(item:*, index:Number, array:Array) { return checkAvailabilityAndTrace(item); }

	// Check if product is available, and trace information about it
	private function checkAvailabilityAndTrace(item:*, enableLog:Boolean = true):Boolean {
		var oldItem = item;
		if (item is String) {
			item = _dataFeed.returnSpecificItem({server_name: item });
		}
		if (item is MenuboardFeedItem || item is MenuboardProduct) {
			if (enableLog) {
				const message = [
					item.server_name || (oldItem is String ? oldItem : "Node was not found"),
					": " + (item.available ? "enabled" : "n/a"),
					(item is MenuboardFeedItem && item.price ? " -> $" + item.price : "")
				];
				log(["	-> "].concat(message).join(""));
			}
			return item.available;
		}
		log("ERROR: checkAvailabilityAndTrace -> Only String, MenuboardFeedItem, or MenuboardProduct should be passed in");
		return false;
	}

	private function modifyLinkageIfIsReduced(linkage:String) {
		if (isReducedLTO()) {
			log("LTO should be reduced!");
			return linkage + REDUCED_SUFFIX;
		}
		return linkage;
	}

	// Only used for cold bev boards, to determine if the LTO is reduced
	private function isReducedLTO():Boolean {
		log("Checking if LTO needs to be the reduced version");

		const espressoTestLondon:Boolean = checkAvailabilityAndTrace("espresso_test_london");
		const espressoProducts:Number = getProductListLength([
			"*_iced_latte_espresso_test",
			"*_iced_mocha_latte_espresso_test",
			"*_iced_vanilla_latte_espresso_test",
			"*_iced_caramel_latte_espresso_test"
		]);
		log("	->	Espresso Products -> " + espressoProducts + "\n	-> London Test -> " + espressoTestLondon);
		
		const newJuiceAvailable:Boolean = multipleAreAvailable(["bottled_juices_orange_new", "bottled_juices_apple_new"]);
		const bottledBevProducts:Number = getProductListLength([
			"large_bottled_soft_drink",
			"bottled_beverages_*_iced_tea",
			"bottled_beverages_gatorade",
			"bottled_beverages_spring_water",
			(newJuiceAvailable ? "bottled_juices_orange_new" : "bottled_juices_orange"),
			(newJuiceAvailable ? "bottled_juices_apple_new" : "bottled_juices_apple"),
			"*_milk_*"
		]);
		log("	-> Bottled Bev Products -> " + bottledBevProducts);

		if (bottledBevProducts > 0 || (espressoProducts > 0 && !espressoTestLondon)) {
			log("Checking for more than 2 cold beverages");
			const coldBevProducts:Number = getProductListLength([
				"*_iced_cappuccino",
				"*_iced_cappuccino_light",
				"*_mocha_iced_cappuccino",
				"*_iced_coffee",
				"iced_coffee",
				"*_creamy_chocolate_chill",
				"*_orange_pineapple_real_fruit_chill",
				"*_frozen_lemonade",
				"*_greek_yogurt_real_fruit_smoothie"
			]);
			log("	-> Cold Bev Products -> " + coldBevProducts);
			return coldBevProducts > 2;
		}
		return false;
	}

	private function getProductListLength(serverNames:Array):Number {
		const createServerNameObject = function (serverName) { return { server_name: serverName }; };
		const createListFromServerNames = function (serverNames:Array) { return serverNames.map(createServerNameObject); };
		
		const productList:MenuboardProductList = new MenuboardProductList();
		productList.createList(_dataFeed, createListFromServerNames(serverNames));
		return productList.length;
	}

	/**
	 * Gather prices from a Menuboard Product into an array of numbers.
	 * Step 1: Get the specified size value (string)
	 * Step 2: Filter out any non-truthy values (ex sizes that don't exist)
	 */
	private function getPricesFromProduct(product:MenuboardProduct, sizes:Array):Array {
		const prices:Array = sizes
			.map(function (size) { return product.prices[size]; })
			.filter(function (size) { return Boolean(size); });
		if (prices.length !== sizes.length) {
			log("WARNING: " + (sizes.length - prices.length) + " of the sizes does not exist on the MenuboardProduct");
		}
		return prices;
	}

	private function createLTOInstance(name:String, prices:Array = null, extras:Object = null, disableLanguage:Boolean = false):DynamicPromo {
		if (_boardSettings.board === BOARD_COLD) {
			name = modifyLinkageIfIsReduced(name);
		}
		const instance:String = name + (disableLanguage ? "" : "_" + _boardSettings.language);
		log("Instanciating LTO: " + instance);

		const ltoInstance = DisplayUtils.instanciateMCByLinkage(instance);
		var errorMsg = null;
		if (!ltoInstance) {
			errorMsg = "check that the MovieClip instance name is " + instance + ".";
		} else if (!(ltoInstance is DynamicPromo)) {
			errorMsg = "MovieClip does not have a base class of DynamicPromo";
		}
		if (errorMsg) {
			throw new Error("Unable to instantiate LTO: '" + instance + "', " + errorMsg);
		}

		if (prices && prices.length !== 0) {
			const priceMCs = DisplayUtils.getChildrenWithName(ltoInstance, DisplayNames.PRICE_NAME, { equal:false, contained:true });
			prices.forEach(function (p, i) {
				if (!priceMCs[i]) {
					log("ERROR: Could not find price movieclip -> [price" + i + "] check naming in " + instance);
				}
			});
			ltoInstance.prices = prices;
			log("	-> Prices: " + prices);
		}
		if (extras) {
			ltoInstance.extras = extras;
		}
		ltoInstance.display();
		return ltoInstance;
	}
}
