# Menuboard and Promo Tools

This repository is a collection of tools, both in development and in use.

It also contains a Template for TDL promos.  In `src/com` there is an experiemental branch of the `dmb_as3` repository.  It is used for the experiemental promos until the code is ready to be pushed into the `master` branch.

## Current Features
- Tool installer
- [Promos](docs/Promos.md) 
    - JSFL publishing script
    - Template for new promos
    - [PromoUtil & PromoHelper](docs/Promos_Experimental.md) from the [dmb_as3](https://bitbucket.org/ek3technologies/dmb_as3/branch/dev/promo_helper)

## Planned Tools
- Promos
    - Convert `publishPromo` to a Flash SWF panel to be used in the IDE
    - Promo generator
        - JSFL Script for generating the AS3 code based on user input
        - JSFL Script for generating the fla's for a new promo, editing all the repetitive info
- Menuboards/All
    - XML Switcher / Test assistant - learn [more](docs/XMLTester.md)
        - JSFL/Flash panel version
        - Standalone Node.js Electron version

## Installation

TODO

## Development

TODO