﻿/**
 * TODO - Cache the getItem() requests, but be sure to clear it after the build is finished
 *				-> Add logging to getItem(), but only if it isn't cached
 * TODO - Add prop in settings object for where to grab calories from ie { calories: [0, "producT_serveR_name"] }!!!!!!!!!!!!!
 * TODO - Add a build.json file to root of promo folder.  Used in conjuction with a publish script
 * TODO - Create a PromoModule class, and put most of the logic in there
 * TODO - in createPromoInstances() check if all movieclips exist for promo linkage, BEFORE trying, stops from showing a default for EN and not for FR?
 * @description
 * A template class to use for building new promos.  This class will handle promos of any size.
 * Template FLA must contain the following Config Constants:
 * 		-> CONFIG::LANGUAGE -> Set to EN, FR, BI
 *		-> CONFIG::SETTINGS -> Misc settings object, takes any JSON string
 * @see https://bitbucket.org/ek3technologies/dmb_as3/branch/promo_helper
 *
 * Publish script and templates can be found here:
 * {@link \\qnap03.ek3.com\LDNClients\Active\WIS\TDL\Promos\_Template_}
 **/
package {

	import com.ek3.dmb.debug.Debugger;
	import com.ek3.dmb.display.DisplayModule;
	import com.ek3.dmb.display.playlists.BasicPlaylist;
	import com.ek3.dmb.external.data.DataStorage;
	import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
	import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
	import com.ek3.dmb.platforms.MediaPlatform;
	import com.ek3.dmb.settings.AppSettings;

	import flash.events.Event;
	import flash.text.TextField;
	import flash.utils.setTimeout;

	public class TDLCAN_Template extends MediaPlatform {
		private const LANGUAGE:String = "Any";
		private const APP_NAME:String = "TDLCAN_Template";
		private const DATA_TIMEOUT:Number = 10 * 1000; // 10 Seconds

		//application info
		private var versionNumber:Number = 0.5;
		private var debugLevel:Number = 1;
		private var menuboardObj:MenuboardV2DataFeed;
		private var dataStorage:DataStorage;
		private var firstTime:Boolean = true;
		
		private var mainModule:PromoModule;
		private var boardSettings:Object = { debug: false };
		private var debugTxtMC:TextField = getDebugText();

		public function TDLCAN_Template():void {
			AppSettings.treatPriorityAsDisplay = true;
			this.language = LANGUAGE;
			this.appName = APP_NAME;
			this.debug(debugLevel);

			const debugger = Debugger.getInstance();
			if (debugger.hasOwnProperty("toggleDebugCallback")) { // Check to make sure function exists before calling, keep until PR is merged
				Debugger.getInstance()["toggleDebugCallback"] = toggleDebugMode;
			} else {
				trace("WARN: Toggle Debug callback does not exist in the Library");
			}

			// Wrap in try-catch so exception can be shown in debug mode
			try {
				//Set up Data Storage, Listeners and vars
				dataStorage = DataStorage.getInstance();
				dataStorage.addEventListener(DataStorage.ALL_DATA_RECEIVED, onDataReady);
				var requiredData:Array = new Array();

				//Create MBV2 DataFeed
				menuboardObj = new MenuboardV2DataFeed(30*1000);
				requiredData.push(menuboardObj);

				// Try to parse Misc settings passed in through the FLA
				var cleanJSON = CONFIG::SETTINGS.replace(/([$])([$])?/g, '"') || '{}';
				boardSettings = JSON.parse(cleanJSON);
				boardSettings.height = stage.stageHeight;
				boardSettings.language = CONFIG::LANGUAGE;
				showDebugMessage('Board Settings', boardSettings);
				boardSettings.debugTxtMC = debugTxtMC;

				//Create Display Modules
				mainModule = new PromoModule(boardSettings);
				mainModule.addDataRequirement(menuboardObj.name);
				this.promoMC.addChild(mainModule);
				mainModule.addEventListener(PromoModule.PROMOPLAYLIST_COMPLETE, exit);

				//Start Loading
				dataStorage.fetchData(requiredData);
				setTimeout(function () {
					if (!boardSettings.dataLoaded) {
						showDebugMessage("Data was not loaded within timeout period, make sure a 'menuboardv2.xml' exists");
					}
				}, DATA_TIMEOUT);
			} catch (ex) {
				trace("FATAL ERROR: During settings parse, or board creation, contact WIS");
				showDebugMessage(ex);
				throw ex;
			}
		}

		private function getDebugText():TextField {
			if (!debugTxtMC) {
				var debugText = new TextField();
				debugText.background = true;
				debugText.backgroundColor = 0x000000;
				debugText.textColor = 0xFFFFFF;
				debugText.height = stage.stageHeight;
				debugText.width = stage.stageWidth;
				debugText.alpha = 0.6;
				debugTxtMC = debugText
			}
			return debugTxtMC;
		}

		private function showDebugMessage(message:String, data:Object = null) {
			if (data) { message += '\n' + JSON.stringify(data, null, 2) };
			if (debugTxtMC) {
				debugTxtMC.appendText(message + "\n");
				debugTxtMC.visible = boardSettings.debug;
			}
		}

		private function toggleDebugMode(enabled:Boolean):void {
			boardSettings.debug = enabled;
			if (boardSettings.debugTxtMC) {
				addChild(boardSettings.debugTxtMC);
			}
			showDebugMessage("Debug mode: " + boardSettings.debug);
		}

		private function onDataReady(e:Event = null):void {
			//Data has loaded but not necessarily changed
			boardSettings.dataLoaded = true;
			if(firstTime) {
				firstTime = false;
				mainModule.build(false, !this.hasPlayEvent);
			}
		}

		public override function onPlay(e:Event = null):void {
			super.onPlay(e);
			if(mainModule && menuboardObj.data) {
				mainModule.build();
			}
		}
	}
}

import com.ek3.dmb.debug.Debugger;
import com.ek3.dmb.display.DisplayModule;
import com.ek3.dmb.display.DynamicPromo;
import com.ek3.dmb.display.playlists.BasicPlaylist;
import com.ek3.dmb.external.data.DataStorage;
import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
import com.ek3.dmb.platforms.MediaPlatform;
import com.ek3.dmb.utils.DisplayUtils;
import com.ek3.dmb.settings.namingconvention.DisplayNames;

import flash.events.Event;
import flash.utils.*;
import flash.events.KeyboardEvent;

internal class PromoModule extends DisplayModule {

	public static var PROMOPLAYLIST_COMPLETE:String = "promomodule_playlist_complete";
	private var videoPlaylist:BasicPlaylist; //Using playlist type: Basic

	private const LANGUAGE_EN = "EN";
	private const LANGUAGE_FR = "FR";
	private const LANGUAGE_BI = "BI";
	private const LANGUAGE_DEFAULT = LANGUAGE_EN;
	private const DEFAULT_PROMO_NAME = "WhyWeBrew";
	
	// To pass custom calorie wrapping pass { calorieSettings: { EN: { prefix: "". suffix: ""} } } to #createPromoInstances
	private const CAL_SETTINGS = {
		EN: { prefix: "", suffix: " Cals" },
		FR: { prefix: "", suffix: " Cal" }
	}

	private var _dataFeed:MenuboardV2DataFeed;
	private var _boardSettings:Object;

	/** ======================================================================
	 * Edit promo information here
	 */
	/** Customize promo here - Ensure promo linkages are suffexed with language tag ex: Promo1_EN, Promo2_FR **/
	private function determineActivePromo():Array {
		// Promo - Main
		const mainPromo:String = "Template";
		const replace2:MenuboardFeedItem = getItem("replace_node2");
		const replace3:MenuboardFeedItem = getItem("replace_node3");

		const replaceIsAvailable:Boolean = multipleAreAvailable(["replace_with_promo_node", replace2, replace3], mainPromo);
		if (replaceIsAvailable) {
			// Pass the MenuboardFeedItems to createPromoInstances(), to enable calories pass { calories: true } as 3rd param
			return createPromoInstances(mainPromo, [replace2, replace3], {});
		}

		// Fallback promo etc

		return displayDefault();
	}
	/** End customization **/
	/* ====================================================================== */

	// TODO handle this differently, currently don't like how it works, it was cobbled together to just "work" needs to be refined
	private function displayDefault(settings:Object = null, overrideDefaultPromo:String = ""):Array {
		// If price isn't explicitly enabled, disable it, as WhyWeBrew has no price
		if (!settings) settings = {};
		if (!settings.hasOwnProperty("price")) settings.price = false;
		settings.calories = _boardSettings.calories;

		// If calories are enabled for this promo, and passed in as settings, then pass "small_coffee" to createPromoInstances()
		return createPromoInstances(overrideDefaultPromo || DEFAULT_PROMO_NAME, ["small_coffee"], settings);
	}

	// Constructor
	public function PromoModule(boardSettings:Object):void {
		videoPlaylist = new BasicPlaylist();
		boardSettings.language = [LANGUAGE_EN, LANGUAGE_FR, LANGUAGE_BI].indexOf(boardSettings.language) !== -1 ? boardSettings.language : LANGUAGE_DEFAULT;
		_boardSettings = boardSettings;
		if (boardSettings.portrait) {
			_boardSettings.language = LANGUAGE_EN;
			log("Enabling portrait mode");
		}
		log("Setting Promo language to -> " + _boardSettings.language);
	}

	override protected function doBuild():void {
		_dataFeed = DataStorage.getInstance().data["menuboardv2"];

		if (_boardSettings.portrait) {
			makePortrait();
		}

		_boardSettings.calories = (checkAvailabilityAndTrace("calories", false)
			&& [LANGUAGE_BI, LANGUAGE_EN].indexOf(_boardSettings.language) !== -1);

		var activePromo:Array = determineActivePromo();
		if (!activePromo) {
			log("No active promo could be found, defaulting to -> " + DEFAULT_PROMO_NAME);
			activePromo = displayDefault();
		}

		// Default could not be created
		if (activePromo.length === 0) {
			log("Failed to instantiate default -> " + DEFAULT_PROMO_NAME + ", defaulting to fallback default");
			activePromo = createPromoInstances("CoffeeQuality");
		}

		// Determine which promo to display, customize above
		videoPlaylist.addPromos(activePromo);

		/********************************PROMO CONDITIONS********************************/
		if(!videoPlaylist.numOfVideos) dispatchEvent(new Event(PROMOPLAYLIST_COMPLETE));

		addChild(videoPlaylist);

		const playLength = ((videoPlaylist.totalFrames / stage.frameRate) * 2);
		MediaPlatform.instance.playLength = playLength; //100% buffer
		log("Promo length (seconds) is -> " + playLength);
		if(!videoPlaylist.hasEventListener(BasicPlaylist.PLAYLIST_COMPLETE)) {
			log("onVideoComplete added");
			videoPlaylist.addEventListener(BasicPlaylist.PLAYLIST_COMPLETE, onVideoComplete);
		}
		/** PROMO PLAYLIST **/
	}

	override public function deconstruct():void {
		super.deconstruct();
		log("ACTIVATE");
		if(videoPlaylist) {
			videoPlaylist.clearPlaylist();
			if(videoPlaylist.parent == this) removeChild(videoPlaylist);
		}
	}

	/* Helper Functions */

	private function getItem(serverName:String):MenuboardFeedItem {
		return _dataFeed.returnSpecificItem({ server_name: serverName });
	}

	private function log(message:String, data:Object = null) {
		var dataString:String = "";
		if (data) dataString = "\n" + JSON.stringify(data, null, 2);
		_boardSettings.debugTxtMC.appendText(message + "\n");
		trace(message);
	}

	private function multipleAreAvailable(items:Array, promoName:String, anyAvailable:Boolean = false): Boolean {
		if (items.length === 0) return false;
		log((promoName ? promoName : "Promo") + " Nodes");
		const requiredLength:Number = anyAvailable ? 1 : items.length;
		const isAvailable:Boolean = items.filter(function (item) { return checkAvailabilityAndTrace(item); }).length >= requiredLength;
		log("	-> " + (anyAvailable ? "ANY" : "ALL") + " are available: " + (isAvailable ? "yes" : "no"));
		return isAvailable;
	}

	private function checkAvailabilityAndTrace(item:*, enableLog:Boolean = true, prefix:String = "	-> "):Boolean {
		var oldItem = item;
		if (item is String) {
			item = _dataFeed.returnSpecificItem({ server_name: item });
		}
		// TODO handle missing price better
		if (item is MenuboardFeedItem) {
			if (enableLog) {
				const message = [
					item.server_name || (oldItem is String ? oldItem : "Node was not found"),
					": " + (item.available ? "enabled" : "n/a"),
					(item.price ? " -> $" + item.price : ""),
					(_boardSettings.calories ? " / " + (item.calories ? item.calories + " cals" : "N/A") : "")
				];
				log([prefix].concat(message).join(""));
			}
			return item.available;
		}
		log("ERROR: checkAvailabilityAndTrace -> Only String, MenuboardFeedItem should be passed in");
		return false;
	}

	/**
	 * Builds promo instances based on language ex: EN -> [Promo1_EN], BI -> [Promo1_EN, Promo1_FR]
	 * Slimmed down version from promo helper branch of dmb_as3
	 * @see https://bitbucket.org/ek3technologies/dmb_as3/branch/promo_helper
	 * @param {Boolean} [settings.price=true] - Enable the showing of prices
	 * @param {Boolean} [settings.calories=false] - Enable the showing of calories
	 * @param {Array}   [settings.extras=null] - Manually control the extras * cannot be used with settings.calories
	 */
	private function createPromoInstances(name:String, items:Array = null, settings:Object = null):Array {
		if (!settings) settings = {};
		if (!settings.hasOwnProperty("price")) settings.price = true;

		const languages:Array = _boardSettings.language === LANGUAGE_BI ? [LANGUAGE_EN, LANGUAGE_FR] : [_boardSettings.language];
		const priceValues:Array = settings.price ? mapFeedItemFromKey("price", items) : [];
		const calorieValues:Array = settings.calories ? mapFeedItemFromKey("calories", items) : [];

		const displayCalories:Boolean = (_boardSettings.portrait && name === DEFAULT_PROMO_NAME) || _boardSettings.calories;

		const validateValues:Function = function (value, index, array) { return !!value; };
		const hasValidPrices:Boolean = priceValues.filter(validateValues).length > 0;
		const hasValidCalories:Boolean = calorieValues.filter(validateValues).length > 0;

		if (items && items.length > 0) { 
			if (settings.price && !hasValidPrices) {
				log("WARNING: Prices are enabled but no price values were able to be pulled from the XML, see logs above");
			}
			if (displayCalories && settings.calories && !hasValidCalories) {
				log("WARNING: Calories are enabled but no calorie values were found in the XML, see logs above");
			}
		}

		if (_boardSettings.portrait && settings.calories && !_boardSettings.calories && name !== DEFAULT_PROMO_NAME) {
			log("WARNING: Is a Portrait board, and the promo has calories, but calories are not enabled...  Displaying default");
			return createPromoInstances(DEFAULT_PROMO_NAME, [], { price: false, calories: false });
		}

		const promoInstances:Array = languages.map(function (currentLanguage) {
			var fullName:String = name + "_";
			if (displayCalories && settings.calories) {
				if (calorieValues.length > 0) {
					fullName += "Cal_";
				} else {
					log("WARNING: Calories are enabled and turned on in the xml but no values could be found, DISABLING calorie version");
				}
			}
			fullName += currentLanguage;

			log("Instanciating promo: " + fullName);
			const promoInstance = DisplayUtils.instanciateMCByLinkage(fullName);
			if (!promoInstance) {
				log("ERROR: Unable to instantiate promo: '" + fullName + "', check the naming of the movie clips");
				log("Defaulting to \"" + DEFAULT_PROMO_NAME + "\"");
				return DisplayUtils.instanciateMCByLinkage(DEFAULT_PROMO_NAME + "_" + currentLanguage);
			}
			promoInstance.name = fullName;
			if (settings.price && priceValues.length !== 0) {
				const pricesToUse:Array = priceValues.slice(0, settings.limitPrices || priceValues.length);
				if (!settings.ignorePriceCheck) {
					const priceMCs = DisplayUtils.getChildrenWithName(promoInstance, DisplayNames.PRICE_NAME, { equal:false, contained:true });
					pricesToUse.forEach(function (p, i) {
						if (p > 0 && !priceMCs[i]) {
							log("WARNING: Could not find price movieclip -> [price" + (i+1) + "] check naming in " + fullName);
						}
					});
				}
				promoInstance.prices = pricesToUse;
				log("	-> Prices: " + pricesToUse);
			}

			if (settings.extras && settings.extras is Array) {
				promoInstance.extras = settings.extras;
			} else if (displayCalories && calorieValues && settings.calories) {
				const calorieSettings:Object = settings.calorieSettings || CAL_SETTINGS;
				const calorieWrap:Object = calorieSettings[currentLanguage] || CAL_SETTINGS[currentLanguage]; // Default to constant
				if (calorieWrap) {
					promoInstance.extras = buildCaloriesExtra(calorieValues, promoInstance, calorieWrap.prefix, calorieWrap.suffix);
				} else {
					log("ERROR: createPromoInstances -> Could not find the calorie wrap for the current language: " + currentLanguage, calorieSettings);
				}
			}
			return promoInstance;
		}).filter(function (promo) { return !!promo; }); // Weed out null instances

		return promoInstances;
	}

	private function mapFeedItemFromKey(key:String, array:Array):Array {
		if (!array) { array = []; }
		return array.map(function (item) {
			if (item is String) {
				item = getItem(item);
			}
			if (item is MenuboardFeedItem) {
				if (!item[key]) {
					log("WARNING: No value was found for [" + key + "] in -> " + item.serverName);
				}
				return item[key];
			}
		});
	}

	// Build the extras array for calories
	private function buildCaloriesExtra(calories:Array, promoInstance:DynamicPromo, prefix:String = "", suffix:String = ""):Array {
		const extras:Array = [];
		if (calories && calories.length > 0) {
			log("Using calories");
			calories.forEach(function (cal, index) {
				const clipName:String = DisplayNames.CALORIES_NAME + (index + 1);
				const calDisplay:String = cal ? prefix + cal + suffix : " ";
				log("	-> " + clipName + " = " + (calDisplay == " " ? "N/A" : calDisplay));
				if (!promoInstance[clipName]) {		
					log("WARNING: Could not find calorie movieclip -> [" + clipName + "] check naming in " + promoInstance.name);		
				}
				extras[clipName] = calDisplay;
			});
		}
		return extras;
	}

	private function makePortrait() {
		videoPlaylist.rotation = -90;
		videoPlaylist.x = 0;
		videoPlaylist.y = _boardSettings.height;
		log("Making board portrait -> " + _boardSettings.height + "px");
	}

	override protected function activate():void {
		super.activate();
		log("ACTIVATE");
		readyToBeUpdated = false;
		videoPlaylist.start();
	}

	private function onVideoComplete(e:Event):void {
		dispatchEvent(new Event(PROMOPLAYLIST_COMPLETE));
		readyToBeUpdated = true;
		log("onVideoComplete: "+readyToBeUpdated);
	}
}
