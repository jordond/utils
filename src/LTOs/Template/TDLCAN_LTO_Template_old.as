﻿/**
 * @description
 * A template class to use for building new transparent LTOs.
 *
 * Add your MovieClips to the FLA, and treat them like a promo.  Prices will be price1, price2, etc.
 * To customize the LTO edit the function "determineActiveLTO".
 * To publish the LTO, you can use the promo publish script found on the server
 * \\qnap03.ek3.com\LDNClients\Active\WIS\TDL\Promos\_Template_\publish.jsfl
 *
 **/
package {

	import com.ek3.dmb.debug.Debugger;
	import com.ek3.dmb.display.DisplayModule;
	import com.ek3.dmb.display.playlists.BasicPlaylist;
	import com.ek3.dmb.external.data.DataStorage;
	import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
	import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
	import com.ek3.dmb.platforms.MediaPlatform;
	import com.ek3.dmb.settings.AppSettings;

	import flash.events.Event;
	import flash.utils.setTimeout;

	public class TDLCAN_LTO_Template extends MediaPlatform {
		private const APP_NAME:String = "TDLCAN_LTO_Template";
		private const DATA_TIMEOUT:Number = 10 * 1000; // 10 Seconds

		//application info
		private var versionNumber:Number = 1;

		//settings
		private var debugLevel:Number = 1;

		private var menuboardObj:MenuboardV2DataFeed;
		private var dataStorage:DataStorage;
		private var boardSettings:Object = { debug: false };

		//Menu Modules
		private var mainModule:LTOModule;

		public function TDLCAN_LTO_Template():void {

			//Settings
			AppSettings.treatPriorityAsDisplay = true;
			this.language = "Any";
			this.appName = APP_NAME;
			this.debug(debugLevel);
			this.playLength = 10;

			// Wrap in try-catch so exception can be shown in debug mode
			try {
				//Set up Data Storage, Listeners and vars
				dataStorage = DataStorage.getInstance();
				dataStorage.addEventListener(DataStorage.ALL_DATA_RECEIVED, onDataReady);
				var requiredData:Array = new Array();

				//Create MBV2 DataFeed
				menuboardObj = new MenuboardV2DataFeed(30*1000);
				requiredData.push(menuboardObj);

				// Try to parse Misc settings passed in through the FLA
				var cleanJSON = CONFIG::SETTINGS.replace(/([$])([$])?/g, '"') || '{}';
				boardSettings = JSON.parse(cleanJSON);
				boardSettings.language = CONFIG::LANGUAGE.toUpperCase();
				showDebugMessage('Board Settings', boardSettings);
				if (boardSettings.debug && this.getChildByName('debugTextField')) {
					boardSettings.debugTxtMC = this['debugTextField'];
				}

				//Create Display Modules
				mainModule = new LTOModule(boardSettings);
				mainModule.addDataRequirement(menuboardObj.name);
				this.displayArea.addChild(mainModule);

				//Start Loading
				dataStorage.fetchData(requiredData);
				setTimeout(function () {
					if (!boardSettings.dataLoaded) {
						showDebugMessage("Data was not loaded within timeout period, make sure a 'menuboardv2.xml' exists");
					}
				}, DATA_TIMEOUT);
			} catch (ex) {
				trace("ERROR: During settings parse, or board creation");
				showDebugMessage(ex);
				throw ex;
			}
		}

		private function showDebugMessage(message:String, data:Object = null) {
			if (data) message += '\n' + JSON.stringify(data, null, 2);
			if (this.getChildByName('debugTextField')) {
				if (boardSettings.debug) this['debugTextField'].text += message + "\n";
				this['debugTextField'].visible = boardSettings.debug;
			}
		}

		private function onDataReady(e:Event = null):void {
			boardSettings.dataLoaded = true;
			mainModule.build();
		}
	}
}

import com.ek3.dmb.display.DisplayModule;
import com.ek3.dmb.display.DynamicPromo
import com.ek3.dmb.external.data.DataStorage;
import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
import com.ek3.dmb.external.data.menuboard.MenuboardProduct;
import com.ek3.dmb.external.data.menuboard.MenuboardProductList;
import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
import com.ek3.dmb.settings.namingconvention.DisplayNames;
import com.ek3.dmb.utils.DisplayUtils;

import flash.display.MovieClip;

internal class LTOModule extends DisplayModule {

	private const LANGUAGE_EN = "EN";
	private const LANGUAGE_FR = "FR";
	private const LANGUAGE_BI = "BI";
	private const LANGUAGE_DEFAULT = LANGUAGE_EN;
	private const REDUCED_SUFFIX = "_Reduced";

	private var _dataFeed:MenuboardV2DataFeed;
	private var _boardSettings:Object;

	public function LTOModule(boardSettings:Object):void {
		const languageIndex = [LANGUAGE_EN, LANGUAGE_FR, LANGUAGE_BI].indexOf(boardSettings.language);
		if (languageIndex === -1) {
			log("WARNING: Invalid language of [" + boardSettings.language + "] was passed in defaulting to -> " + LANGUAGE_DEFAULT);
			boardSettings.language = LANGUAGE_DEFAULT;
		}
		_boardSettings = boardSettings;
		log("Setting LTO language to -> " + _boardSettings.language);
	}

	/** ======================================================================
	 * All LTO selection logic will go in here
	 * Ensure lto linkages are suffexed with language tag ex: AwesomeLTO_EN, AwesomeLTO_FR
	 */
	private function determineActiveLTO():DynamicPromo {
		// Main LTO
		var ltoLinkageName:String = "Template";

		var replace1:MenuboardFeedItem = _dataFeed.returnSpecificItem({server_name: "replace1" });
		var replace2:MenuboardFeedItem = _dataFeed.returnSpecificItem({server_name: "replace2" });
		var replace3:MenuboardFeedItem = _dataFeed.returnSpecificItem({server_name: "replace3" });

		var replaceIsAvailable:Boolean = multipleAreAvailable([replace1, replace2, replace3], ltoLinkageName);

		if (replaceIsAvailable) {
			return createLTOInstance(ltoLinkageName, [replace2.price, replace3.price]);
		}

		// Add additional LTO checks here

		/** End customization **/
		return null;
	}
	/**
	 * End of LTO selection logic
	 * ======================================================================
	 */

	override protected function doBuild():void {
		_dataFeed = DataStorage.getInstance().data["menuboardv2"];

		// Create the layout and attach it
		const boardContainer:MovieClip = new LTOLayout();
		addChild(boardContainer);

		try {
			// Determine which promo to display then attach it to the layout
			var activeLTO:DynamicPromo = determineActiveLTO();
			if (!activeLTO) {
				activeLTO = createLTOInstance("Default", null, null, true);
			}
			boardContainer.ltoHolder.addChild(activeLTO);
		} catch (err) {
			log(err.message, err.getStackTrace());
			throw err;
		}
	}
	
	/* Helper functions */

	private function log(message:String, data:Object = null) {
		var dataString:String = "";
		if (data) dataString = "\n" + JSON.stringify(data, null, 2);
		if (_boardSettings.debug && _boardSettings.debugTxtMC) {
			_boardSettings.debugTxtMC.text += "\n" + message + dataString;
		}
		trace(message);
	}

	private function multipleAreAvailable(items:Array, name:String = null): Boolean {
		if (name) {
			log(name + " Nodes");
		}
		const allItemsAvailable:Boolean = items.filter(function (item) { return checkAvailabilityAndTrace(item); }).length === items.length;
		log("	-> Available: " + (allItemsAvailable ? "yes" : "no"));
		return allItemsAvailable;
	}

	private function checkAvailabilityAndTrace(item:*):Boolean {
		if (item is String) {
			item = _dataFeed.returnSpecificItem({server_name: item });
		}
		if (item is MenuboardFeedItem || item is MenuboardProduct) {
			const message = [
				item.server_name,
				": " + (item.available ? "enabled" : "n/a"),
				(item is MenuboardFeedItem && item.price ? " -> $" + item.price : "")
			];
			log(["	-> "].concat(message).join(""));
			return item.available;
		}
		log("ERROR: checkAvailabilityAndTrace -> Only String, MenuboardFeedItem, or MenuboardProduct should be passed in");
		return false;
	}

	private function modifyLinkageIfIsReduced(linkage:String) {
		if (isReducedLTO()) {
			log("Using a reduced LTO");
			return linkage + REDUCED_SUFFIX;
		}
		return linkage;
	}

	// Only used for cold bev boards, to determine if the LTO is reduced
	private function isReducedLTO():Boolean {
		log("Checking if LTO needs to be the reduced version");

		const espressoTestLondon:Boolean = checkAvailabilityAndTrace("espresso_test_london");
		const espressoProducts:Number = getProductListLength([
			"*_iced_latte_espresso_test",
			"*_iced_mocha_latte_espresso_test",
			"*_iced_vanilla_latte_espresso_test",
			"*_iced_caramel_latte_espresso_test"
		]);
		log("	->	Espresso Products -> " + espressoProducts + "\n	-> London Test -> " + espressoTestLondon);
		
		const newJuiceAvailable:Boolean = multipleAreAvailable(["bottled_juices_orange_new", "bottled_juices_apple_new"]);
		const bottledBevProducts:Number = getProductListLength([
			"large_bottled_soft_drink",
			"bottled_beverages_*_iced_tea",
			"bottled_beverages_gatorade",
			"bottled_beverages_spring_water",
			(newJuiceAvailable ? "bottled_juices_orange_new" : "bottled_juices_orange"),
			(newJuiceAvailable ? "bottled_juices_apple_new" : "bottled_juices_apple"),
			"*_milk_*"
		]);
		log("	-> Bottled Bev Products -> " + bottledBevProducts);

		if (bottledBevProducts > 0 || (espressoProducts > 0 && !espressoTestLondon)) {
			log("Checking for more than 2 cold beverages");
			const coldBevProducts:Number = getProductListLength([
				"*_iced_cappuccino",
				"*_iced_cappuccino_light",
				"*_mocha_iced_cappuccino",
				"*_iced_coffee",
				"iced_coffee",
				"*_creamy_chocolate_chill",
				"*_orange_pineapple_real_fruit_chill",
				"*_frozen_lemonade",
				"*_greek_yogurt_real_fruit_smoothie"
			]);
			log("	-> Cold Bev Products -> " + coldBevProducts);
			return coldBevProducts > 2;
		}
		return false;
	}

	private function getProductListLength(serverNames:Array):Number {
		const createServerNameObject = function (serverName) { return { server_name: serverName }; };
		const createListFromServerNames = function (serverNames:Array) { return serverNames.map(createServerNameObject); };
		
		const productList:MenuboardProductList = new MenuboardProductList();
		productList.createList(_dataFeed, createListFromServerNames(serverNames));
		return productList.length;
	}

	/**
	 * Gather prices from a Menuboard Product into an array of numbers.
	 * Step 1: Get the specified size value (string)
	 * Step 2: Filter out any non-truthy values (ex sizes that don't exist)
	 */
	private function getPricesFromProduct(product:MenuboardProduct, sizes:Array):Array {
		const prices:Array = sizes
			.map(function (size) { return product.prices[size]; })
			.filter(function (size) { return Boolean(size); });
		if (prices.length !== sizes.length) {
			log("WARNING: " + (sizes.length - prices.length) + " of the sizes does not exist on the MenuboardProduct");
		}
		return prices;
	}

	private function createLTOInstance(name:String, prices:Array = null, extras:Object = null, disableLanguage:Boolean = false):DynamicPromo {
		const instance:String = name + (disableLanguage ? "" : "_" + _boardSettings.language);
		log("Instanciating LTO: " + instance);

		const ltoInstance = DisplayUtils.instanciateMCByLinkage(instance);
		var errorMsg = null;
		if (!ltoInstance) {
			errorMsg = "check that the MovieClip instance name is " + instance + ".";
		} else if (!(ltoInstance is DynamicPromo)) {
			errorMsg = "MovieClip does not have a base class of DynamicPromo";
		}
		if (errorMsg) {
			throw new Error("Unable to instantiate LTO: '" + instance + "', " + errorMsg);
		}

		if (prices && prices.length !== 0) {
			const priceMCs = DisplayUtils.getChildrenWithName(ltoInstance, DisplayNames.PRICE_NAME, { equal:false, contained:true });
			prices.forEach(function (p, i) {
				if (!priceMCs[i]) {
					log("ERROR: Could not find price movieclip -> [price" + i + "] check naming in " + instance);
				}
			});
			ltoInstance.prices = prices;
			log("	-> Prices: " + prices);
		}
		if (extras) {
			ltoInstance.extras = extras;
		}
		ltoInstance.display();
		return ltoInstance;
	}
}
