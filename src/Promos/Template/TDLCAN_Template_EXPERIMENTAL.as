﻿/**
 * @description
 * A template class to use for building new promos.  This class will handle promos of any size.
 * Template FLA must contain the following Config Constants:
 * 		-> CONFIG::LANGUAGE -> Set to EN, FR, BI
 *		-> CONFIG::SETTINGS -> Misc settings object, takes any JSON string
 *
 * This template was the starting point, and thus is a slimmed down version of a PromoBuilder & PromoUtils class on
 * the "promo_helper" branch of the dmb_as3 repository
 * @see https://bitbucket.org/ek3technologies/dmb_as3/branch/promo_helper
 **/
package {

	import com.ek3.dmb.debug.Debugger;
	import com.ek3.dmb.display.DisplayModule;
	import com.ek3.dmb.display.playlists.BasicPlaylist;
	import com.ek3.dmb.external.data.DataStorage;
	import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
	import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
	import com.ek3.dmb.platforms.MediaPlatform;
	import com.ek3.dmb.settings.AppSettings;

	import flash.events.Event;

	public class TDLCAN_Template extends MediaPlatform
	{
		private var LANGUAGE:String = "Any";
		private var APP_NAME:String = "TDLCAN_Template";

		//application info
		private var versionNumber:Number = 0.5;

		//settings
		private var debugLevel:Number = 1;

		private var menuboardObj:MenuboardV2DataFeed;
		private var dataStorage:DataStorage;
		private var firstTime:Boolean = true;
		private var boardSettings:Object = { debug: true };

		//Menu Modules
		private var mainModule:PromoModule;

		public function TDLCAN_Template():void {

			//Settings
			AppSettings.treatPriorityAsDisplay = true;
			this.language = LANGUAGE;
			this.appName = APP_NAME;
			this.debug(debugLevel);

			// Wrap in try-catch so exception can be shown in debug mode
			try {
				//Set up Data Storage, Listeners and vars
				dataStorage = DataStorage.getInstance();
				dataStorage.addEventListener(DataStorage.ALL_DATA_RECEIVED, onDataReady);
				var requiredData:Array = new Array();

				//Create MBV2 DataFeed
				menuboardObj = new MenuboardV2DataFeed(30*1000);
				requiredData.push(menuboardObj);

				// Try to parse Misc settings passed in through the FLA
				var cleanJSON = CONFIG::SETTINGS.replace(/([$])([$])?/g, '"') || '{}';
				boardSettings = JSON.parse(cleanJSON);
				boardSettings.height = stage.stageHeight;
				boardSettings.language = CONFIG::LANGUAGE;
				showDebugMessage('Board Settings: ' + boardSettings);

				//Create Display Modules
				mainModule = new PromoModule(boardSettings);
				mainModule.addDataRequirement(menuboardObj.name);
				this.promoMC.addChild(mainModule);
				mainModule.addEventListener(PromoModule.PROMOPLAYLIST_COMPLETE, exit);

				//Start Loading
				dataStorage.fetchData(requiredData);
			} catch (ex) {
				trace("ERROR: During settings parse, or board creation");
				showDebugMessage(ex);
				throw ex;
			}
		}

		private function showDebugMessage(message:String, data:Object = null) {
			if (data) message += '\n' + JSON.stringify(message, null, 2);
			if (this.getChildByName('debugTextField')) {
				if (boardSettings.debug) this['debugTextField'].text = message;
				this['debugTextField'].visible = boardSettings.debug;
			}
		}

		private function onDataReady(e:Event = null):void {
			//Data has loaded but not necessarily changed
			if(firstTime) {
				firstTime = false;
				mainModule.build(false, !this.hasPlayEvent);
			}
		}

		public override function onPlay(e:Event = null):void {
			super.onPlay(e);
			if(mainModule && menuboardObj.data) {
				mainModule.build();
			}
		}
	}
}

import com.ek3.dmb.display.DisplayModule;
import com.ek3.dmb.display.DynamicPromo;
import com.ek3.dmb.display.playlists.BasicPlaylist;
import com.ek3.dmb.external.data.DataStorage;
import com.ek3.dmb.external.data.menuboard.MenuboardFeedItem;
import com.ek3.dmb.external.data.menuboard.MenuboardV2DataFeed;
import com.ek3.dmb.platforms.MediaPlatform;
import com.ek3.dmb.utils.DisplayUtils;

import flash.events.Event;
import flash.utils.*;
import flash.events.KeyboardEvent;

internal class PromoModule extends DisplayModule {

	public static var PROMOPLAYLIST_COMPLETE:String = "promomodule_playlist_complete";
	private var videoPlaylist:BasicPlaylist; //Using playlist type: Basic

	private const LANGUAGE_EN = "EN";
	private const LANGUAGE_FR = "FR";
	private const LANGUAGE_BI = "BI";
	private const LANGUAGE_DEFAULT = LANGUAGE_EN;

	private var _dataFeed:MenuboardV2DataFeed;
	private var _language:String;
	private var _boardSettings:Object;

	public function PromoModule(boardSettings:Object):void {
		videoPlaylist = new BasicPlaylist();
		boardSettings.language = [LANGUAGE_EN, LANGUAGE_FR, LANGUAGE_BI].indexOf(boardSettings.language) !== -1 ? boardSettings.language : LANGUAGE_DEFAULT;
		if (boardSettings.portrait) {
			boardSettings.language = LANGUAGE_EN;
			trace("Enabling portrait mode");
		}
		_boardSettings = boardSettings;
		trace("Setting Promo language to -> " + boardSettings.language);
	}

	override public function deconstruct():void {
		super.deconstruct();
		trace("ACTIVATE");
		if(videoPlaylist) {
			videoPlaylist.clearPlaylist();
			if(videoPlaylist.parent == this) removeChild(videoPlaylist);
		}
	}

	function isMultipleAvailableWithPriority(items:Array): Boolean {
		var availableItems = items.filter(function (item) { return item.available; });
		return availableItems.length === items.length;
	}

	function printItemStatus(item:MenuboardFeedItem):String {
		return item.available ? "enabled" : "n/a";
	}

	/**
	 * Builds promo instances based on language ex: EN -> [Promo1_EN], BI -> [Promo1_EN, Promo1_FR]
	 * Slimmed down version from promo helper branch of dmb_as3
	 * @see https://bitbucket.org/ek3technologies/dmb_as3/branch/promo_helper
	 */
	private function createPromoInstances(name:String, prices:Array = null):Array {
		const languages:Array = _boardSettings.language === LANGUAGE_BI ? [LANGUAGE_EN, LANGUAGE_FR] : [_boardSettings.language];
		const promoInstances:Array = languages.map(function (currentLanguage) {
			const instance:String = name + "_" + currentLanguage;
			trace("Instanciating promo: " + instance);
			const promoInstance = DisplayUtils.instanciateMCByLinkage(instance);
			if (prices && prices.length !== 0) {
				promoInstance.prices = prices;
				trace("	-> Prices: " + prices);
			}
			return promoInstance;
		});
		return promoInstances;
	}

	private function makePortrait() {
		videoPlaylist.rotation = -90;
		videoPlaylist.x = 0;
		videoPlaylist.y = _boardSettings.height;
		trace("Making board portrait -> " + _boardSettings.height + "px");
	}

	override protected function doBuild():void {
		_dataFeed = DataStorage.getInstance().data["menuboardv2"];

		if (_boardSettings.portrait) {
			makePortrait();
		}

		/** Customize promo here - Ensure promo linkages are suffexed with language tag ex: Promo1_EN, Promo2_FR **/

		var template1:MenuboardFeedItem = _dataFeed.returnSpecificItem({server_name: "template1" });
		var template2:MenuboardFeedItem = _dataFeed.returnSpecificItem({server_name: "template2" });
		var template3:MenuboardFeedItem = _dataFeed.returnSpecificItem({server_name: "template3" });

		var templateIsAvailable:Boolean = isMultipleAvailableWithPriority([template1, template2, template3]);

		trace([
			"Template Promo Nodes",
			"	-> " + template1.server_name + ": " + printItemStatus(template1),
			"	-> " + template2.server_name + ": " + printItemStatus(template2) + " -> " + template2.price,
			"	-> " + template3.server_name + ": " + printItemStatus(template3) + " -> " + template3.price,
			"	-> Available: " + (templateIsAvailable ? "yes" : "no")
		].join("\n"));

		if (templateIsAvailable) {
			videoPlaylist.addPromos(createPromoInstances("Template", [template2.price, template3.price]));
		}  else {
			videoPlaylist.addPromos(createPromoInstances("CoffeeQuality"));
		}

		/** End customization **/

		/********************************PROMO CONDITIONS********************************/
		if(!videoPlaylist.numOfVideos) dispatchEvent(new Event(PROMOPLAYLIST_COMPLETE));

		addChild(videoPlaylist);
		MediaPlatform.instance.playLength = ((videoPlaylist.totalFrames / stage.frameRate) * 2); //100% buffer
		trace("time set");
		if(!videoPlaylist.hasEventListener(BasicPlaylist.PLAYLIST_COMPLETE)) {
			trace("onVideoComplete added");
			videoPlaylist.addEventListener(BasicPlaylist.PLAYLIST_COMPLETE, onVideoComplete);
		}
		/** PROMO PLAYLIST **/
	}

	override protected function activate():void {
		super.activate();
		trace("ACTIVATE");
		readyToBeUpdated = false;
		videoPlaylist.start();
	}

	private function onVideoComplete(e:Event):void {
		dispatchEvent(new Event(PROMOPLAYLIST_COMPLETE));
		readyToBeUpdated = true;
		trace("onVideoComplete: "+readyToBeUpdated);
	}
}
