var version = '0.0.1a';
var author = 'Jordon de Hoog';

if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

var fl = fl;
var _logs = [];
function log(message) {
    _logs.push(formatConsoleDate() + message);
    fl.trace(formatConsoleDate() + message);
}
var scriptStartDate = new Date();
log([
    '',
    'AutoPublisher - Promo creation script',
    'Version: v' + version,
    'Author : ' + author,
    'Date   : ' + scriptStartDate.toDateString() + ' ' + scriptStartDate.toLocaleTimeString()
].join('\n'));

var promo_DEFAULT_NAME = "Template";

var validate_EMPTY_INPUT = "The name was empty";
var validate_HAS_SPECIAL = "The name must not contain any special characters";
var validate_START_LETTER = "The name must start with a letter not a number";
var validate_IS_DEFAULT = "The name cannot be '" + promo_DEFAULT_NAME + "'";

fl.outputPanel.clear();
log("Starting");

// TODO look into a way to import the image assets as well
    // IE have file open
    // Point to asset folder
    // Recursively scan all files,
    // Find matching
        // If multiple or not found
            // have button to manually select
// Long time out

// Gather Info
    // Get the promo name from the user Y
        // Ask if it should be filename as well Y
            // NO Y
                // Get filename Y
    // Validate the name Y
        // If not valid, prompt for new name Y
    // Format the name (remove all whitespace, capitalize words, etc) Y

    // Get the location of the template files (show default server path, have button for local) from user
    // (Save location to temporary file, allow to reset the location back to server)
        // Check to make sure it exists
        // Get a list of files to copy
    // Get the destination folder from user
    // Needs calories?
    // Output a report of what is about to happen
        // Filenames, etc
    
// Do work
    // Create subfolder with Promo Name
    // Copy the selected template files to folder, renaming to include PromoName
    // Prompt to ask if it should edit all files and rename to promo name
        // Yes
            // For each FLA
                // Rename the docClass
                // Remove calorie movie clips if not needed
                // Iterate through all library items and replace "Template" with "PromoName"
                    // Ensure all movieclips are exported for AS and are a DynamicPromo
                // Save and close
    // Prompt whether or not to open files

var _currentDOM = fl.getDocumentDOM();

var _promoName = "";
var _promoFileName = "";

if (!_currentDOM) {
    log('No source FLA file is open')
    confirm('No file is open, to continue you must open ANY fla file, it doesn\'t matter which file you choose.');
    if (confirm) {
        var file = fl.browseForFileURL('open', 'Which file would you like to open?');
        if (file) {
            _currentDOM = fl.openDocument(file);
        }
    }
}

if (_currentDOM) {
    init();
} else {
    log('No valid FLA was found or chosen');
}

function init() {
    var success = getPromoName("");
    if (!success) {
        log("Exiting without doing anything");
        return;
    }
    alert("Promo name is: " + _promoName + "\nFilename is: " + _promoFileName);

}

function getPromoName(error) {
    var result = buildPromoNamePrompt(error);
    if (result) {
        var validated = validateNameInput(result.name);
        if (validated.valid) {
            _promoName = formatPromoName(result.name);
            log("Chosen promo name is -> " + _promoName);
            if (result.sameFilename !== 'true' && result.filename) {
                _promoFileName = formatPromoName(result.filename);
            } else {
                _promoFileName = _promoName;
            }
            return true;
        } else {
            log("Invalid promo name: " + validated.message);
            return getPromoName(validated.message);
        }
    }
    return false;
}

function buildPromoNamePrompt(error) {
    var s = "";
    s += '<dialog title="Enter Promo Name" buttons="accept, cancel">';
    s +=    '<script>';
    s +=        'function toggleFileName() {';
    s +=            'fl.xmlui.setVisible("filename", fl.xmlui.get("sameFilename") !== "true");';
    s +=            'if (fl.xmlui.get("name")) {';
    s +=                'if (fl.xmlui.get("sameFilename") !== "true")'
    s +=                    'fl.xmlui.set("filename", fl.xmlui.get("name"));';
    s +=            '}';
    s +=        '}';
    s +=        'function hideOnCreate(id) { fl.xmlui.setVisible(id, false); }',
    s +=    '</script>';
    s +=    '<vbox>';
    s +=        '<label value="Enter a valid Promo Name:"></label>';
    s +=        '<textbox id="name" multiline="false" value="" />';
    s +=        '<separator />';
                if (error) {
    s +=            '<label width="300" value="ERROR: ' + error + '"></label>';
                }
    s +=        '<checkbox id="sameFilename" label="Use Promo Name as file name" checked="true" onchange="toggleFileName()" />';
    s +=        '<textbox id="filename" multiline="false" value="" oncreate="hideOnCreate(\'filename\')" />';
    s +=    '</vbox>';
    s += '</dialog>';
    return showDialogForResult('TEMP_promoNamePrompt', s);
}

function showDialogForResult(xmlName, xmlData) {
    var dialogPath = fl.configURI + '/' + xmlName + '.xml';
    FLfile.write(dialogPath, xmlData);
    var result = _currentDOM.xmlPanel(dialogPath);
    return result.dismiss === 'accept' ? result : false;
}


// Return true if the input is a valid class name
function validateNameInput(input) {
    var reject = function (msg) { return { valid: false, message: msg }; };
    // Check for empty string
    if (!input) {
        return reject(validate_EMPTY_INPUT);
    } else {
        input = input.trim();
        if (!input) {
            return reject(validate_EMPTY_INPUT);
        }
    }

    // Must not have any special characters
    if (new RegExp("[^a-zA-Z0-9_. ]", "g").test(input)) {
        return reject(validate_HAS_SPECIAL);
    }

    // Must start with a letter
    if (!(new RegExp("^[a-z]", "i").test(input))) {
        return reject(validate_START_LETTER);
    }

    // Must not be the same as the default
    if (new RegExp(promo_DEFAULT_NAME, "gi").test(input)) {
        return reject(validate_IS_DEFAULT);
    }

    return { valid: true, message: "" };
}

function formatPromoName(original) {
    return original
        .trim()
        .replace(/\b\w/g, function (letter) { return letter.toUpperCase(); })
        .replace(/ /g, "_");
}

// Log util functions
function formatConsoleDate() {
    return '[' + formatDate(':', false, true) + '] ';
}

function formatDate(separator, showDay, showSeconds) {
    var date = new Date();
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var milliSeconds = date.getMilliseconds();
    return [
        (showDay ? (day < 10) ? '0' + day : day + monthMap[date.getMonth()] + separator : ''),
        ((hour < 10) ? '0' + hour: hour) + separator,
        ((minutes < 10) ? '0' + minutes: minutes),
        (showSeconds ? separator + ((seconds < 10) ? '0' + seconds : seconds) : ''),
        (showSeconds ? separator + (Math.floor(milliSeconds / 10)) : '')
    ].join('');
}

function stringify(value) {
    var toString = Object.prototype.toString;
    var isArray = Array.isArray || function (a) { return toString.call(a) === '[object Array]'; };
    var escMap = {'"': '\\"', '\\': '\\\\', '\b': '\\b', '\f': '\\f', '\n': '\\n', '\r': '\\r', '\t': '\\t'};
    var escFunc = function (m) { return escMap[m] || '\\u' + (m.charCodeAt(0) + 0x10000).toString(16).substr(1); };
    var escRE = /[\\"\u0000-\u001F\u2028\u2029]/g;
    if (value == null) {
        return 'null';
    } else if (typeof value === 'number') {
        return isFinite(value) ? value.toString() : 'null';
    } else if (typeof value === 'boolean') {
        return value.toString();
    } else if (typeof value === 'object') {
        if (typeof value.toJSON === 'function') { return stringify(value.toJSON()); }
        else if (isArray(value)) {
            var res = '[';
            for (var i = 0; i < value.length; i++)
                res += (i ? ', ' : '') + stringify(value[i]);
            return res + ']';
        } else if (toString.call(value) === '[object Object]') {
            var tmp = [];
            for (var k in value) {
                if (value.hasOwnProperty(k)) tmp.push(stringify(k) + ': ' + stringify(value[k]));
            }
            return '{' + tmp.join(', ') + '}';
        }
    }
    return '"' + value.toString().replace(escRE, escFunc) + '"';
}