# LTOs

## Paths

- Base folder `\\qnap03.ek3.com\LDNClients\Active\WIS\TDL\LTOs`
- Template files `\\qnap03.ek3.com\LDNClients\Active\WIS\TDL\LTOs\_Template_`
- Publish script (optional) `\\qnap03.ek3.com\LDNClients\Active\WIS\TDL\Promos\_Template_`
(Publish script is the same for promos and LTOs)
- Testing folder `\\qnap03.ek3.com\LDNClients\Active\WIS\TDL\_Testing_\_LTOs_`

## Info

**Note: The LTO template, and process isn't 100% figured out yet.  While what I currently have works, my goal is to clean and refine the process a bit more.**

The included files make it easier to build and ship LTO files quickly.

The template files includes the Generic LTOs for each of the different boards, and the logic that controls them.  

Unlike the promo template, the LTO template does not yet currently handle calories.  They can be manually added in and setup, you can see an example of this by looking at the `TDLCAN_LTO_Wedges` files.

## Creating A new LTO

1. Create a copy of the `Template` folder
1. Rename the folder and all of the fla's
1. In the AS3 file replace all instances of `TDLCAN_Template` with the name of your LTO
1. Open the FLA
1. Change the `CONFIG::BOARD` constant (see below)
1. Update the `Document Class` to match the AS3 file
1. Rename the `Template` folder and movie clips **NOTE:** Ensure you leave the language tag
1. Right click the `[Size]_LTOImage.jpg`, and import the proper asset
1. Repeat the same for the `REF_[Size]_LTOReference.jpg`
1. If needed add pricing and other items, such as calories (price1, price2 etc)
1. Now open the ActionScript file, and continue reading below

**NOTE** For now the BI LTOs are the same as EN, so you can safely delete the BI items from the FLA.

__NOTE:__ When running the SWF if you type `DEBUG T` it will show helpful information to the screen.

## LTO Rules

- **Hot Beverages & Cold Beverages**
    - LTO will not show if the following nodes are on:
        1. "calories"
        1. "espresso_test_vancouver"
        1. "espresso_test_london"
        1. "espresso_rollout_bc" ***Hot only***
- **Lunch**
    - Is allowed to have calories
    - Generic changes based on product availability
- **Cold Beverages**
    - If there are bottled beverges enabled, then the LTO will shrink to a smaller Size
    - BE SURE TO INCLUDE THE PROPER MovieClip - See `TDLCAN_LTO_RealFruitChill`
        - ex: `PromoName_EN` && `PromoName_Reduced_EN`

## Structure

### Language, Board type, and Settings

In order to get a different language for the FLA you must set the proper language string in the ActionScript Settings. This is done by  going to 

`File < ActionScript Settings < Config Constants`

And changing `CONFIG::LANGUAGE` to any of the three languages `EN, FR, or BI`. 
If the language does not match it will default to `EN`, **NOTE** `BI` defaults to `EN` as well.

***WARNING***

You **MUST** also change the `CONFIG:BOARD` to either `LUNCH`, `COLD`, `HOT` or `NONE`.  This will ensure the proper rules are followed, and the LTO will behave properly.
As an example the COLD and HOT boards will not show the LTO if calories are enabled.

There is also another constant set called `CONFIG::SETTINGS`.This constant accepts any **valid** JSON string.
This setting can be used to set many different purposes, for example if you need to handle something for only a specific file. 
For the portrait boards you must add a flag in the `CONFIG::SETTINGS` object.

Here is an example of `CONFIG:SETTINGS`.

```javascript
{
    // Enables portrait mode, which forces the LANGUAGE to EN, and rotates the board
    "portrait": true,

    // Allows you to do debug related tasks, it also prints any exceptions to the stage
    "debug": false,

    // You can add anything you want, and handle it in the AS3 code
    "miscSetting": "I am an extra setting"
}
```

In the AS3 code the `CONFIG::LANGUAGE` and `CONFIG::SETTINGS` can be accessed as `_boardSettings`.

## Structure

### Movie Clips

There is a `Template_EN` and `Template_FR` MovieClip available for easy editing.  You just need to update the image placeholder with the proper asset, the MovieClip has existing layers for the background and the reference.

The FLA should have the following Movie Clip structure.

```text
-LTOs
 |-- Template
 | |--- EN
 | | |--- __REF_[Size]_PromoReference_EN.jpg
 | | |--- _[Size]_PromoImage_EN.jpg
 | | |--- Template_EN <-- MovieClip
 | |--- FR
 |   |--- __REF_[Size]_PromoReference_FR.jpg
 |   |--- _[Size]_PromoImage_FR.jpg
 |   |--- Template_FR <-- MovieClip
```

_**NOTE:**_ The promo MovieClips **need** to be of class `com.ek3.dmb.display.AnimatingObject` or `com.ek3.dmb.display.DynamicPromo`

_NOTE:_ Since the playlist is dynamically created, there will be **no** compile error if a linkage does not exist, only a run-time error will be logged, and default will play instead.  So make sure you label them correctly.

### ActionScript

The provided `TDLCAN_LTO_Template.as` is ready to handle and build the LTO as long as the structure above is followed.
Inside the AS3 file you will find a comment informing you were you need to make changes to the file.

Look for the function `determineActiveLTO()` this is where you will check for the existence of your nodes, or any other additional logic.  
Make sure to return a `DynamicPromo`.  If nothing gets returned, it will attempt to show a generic.

Here is an example of the boilerplate code that needs to be changed:

```javascript
private function determineActiveLTO():DynamicPromo {
    // Main LTO
    var ltoLinkageName:String = "Template";
    var replace2:MenuboardFeedItem = getItem("node");
    var replace3:MenuboardFeedItem = getItem("node");

    // multipleAreAvailable takes an array of MenuboardFeedItems or serverNames as a String.
    // It will return true if all of the items in the array are available in the XML
    var replaceIsAvailable:Boolean = multipleAreAvailable(["lto_node_goes_here", replace2, replace3], ltoLinkageName);
    if (replaceIsAvailable) {
        return createLTOInstance(ltoLinkageName, [replace2.price, replace3.price]);
    }

    // If no LTO's are available, show a generic LTO based on the current board (_boardSettings.board)
    // Note: You can override by passing specific generic constant ie: displayGeneric(BOARD_LUNCH)
    // Setting to BOARD_NONE will hide the generic all together (completely transparent)
    return displayGeneric();
}
```

## Publishing

Publishing can be done by changing the `CONFIG::LANGUAGE` to your desired language, export, then rename the SWF. Or the recommended way is to use `publish.jsfl` from the template directory.

The script will handle all of the naming and exporting of the files.

**NOTE:** If you run the script once, then it will copy itself to your commands folder, so you can just click `Commands > run_publish`

Script steps

1. File selection window
    - Scans the current active directory
    - Lists all of the FLA's it finds
    - Can change the directory to find different LTOs
1. Choose which files you wish to publish
1. Options Window
    1. Choose the language you wish to publish
    1. Choose which board type the LTO will be playing on
    1. Enable debug mode
        - Useful when testing the file, display helpful info
    1. Other settings
        - Add custom settings to be passed to each promo **NOTE** must be a valid JSON string
1. Hit okay
1. Each file will be opened, published with language(s) then closed
1. SWFs and log file will be saved to `./SWF` or the user specified folder

## Misc

I currently do not have a set way of adding calories to the LTO, but here is one I have done that included calories.  I made the calories a separate frame.

```javascript
private function determineActiveLTO():DynamicPromo {
    // English Test - Teryiaki
    const teriakiName:String = "TeriyakiWraps";
    const teriakiEach:MenuboardFeedItem = getItem("each_teriyaki_chicken_wrap");
    const teriakiMeal:MenuboardFeedItem = getItem("wedgescombo_teriyaki_chicken_wrap");

    const teriakiIsAvailable = multipleAreAvailable(["lto_teriyaki_wrap", teriakiEach, teriakiMeal], teriakiName);
    if (_boardSettings.language === LANGUAGE_EN && teriakiIsAvailable) {
        return createLTOInstance(teriakiName, [teriakiEach.price, teriakiMeal.price]);
    }

    // Main LTO
    var ltoLinkageName:String = "Wedges";
    var eachPotatoWedges:MenuboardFeedItem = getItem("potato_wedges");

    var wedgesIsAvailable:Boolean = multipleAreAvailable(["lto_potato_wedges", eachPotatoWedges], ltoLinkageName);
    if (wedgesIsAvailable) {
        const extras = [];
        extras["calories1"] = eachPotatoWedges.calories + " Cals";
        const wedgesLto:DynamicPromo = createLTOInstance(ltoLinkageName, [eachPotatoWedges.price], extras);
        if (_isCalories && _boardSettings.language === LANGUAGE_EN) { // TODO add to createLTOInstance
            wedgesLto.gotoAndStop("calories");
        }
        return wedgesLto;
    }

    return displayGeneric();
}
```