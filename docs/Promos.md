# Promos

## Paths

Starting template files as well as the publishing script can be found here: `\\qnap03.ek3.com\LDNClients\Active\WIS\TDL\Promos\_Template_`

## Creating new Promo

1. Create a copy of the `Template` folder
1. Delete the FLA's of the promo sizes you don't need
1. Rename the folder and all of the fla's
1. In the AS3 file replaces all instances of `TDLCAN_Template` with the name of your promo
1. Open the FLA you want to work on
1. Update the `Document Class` to match the AS3 file
1. Rename the `Template` folder and movie clips **NOTE:** Ensure you leave the language tag
1. Right click the `[Size]_PromoImage.jpg`, and import the proper asset
1. Repeat the same for the `REF_[Size]_PromoReference.jpg`
1. If needed add pricing and other items
1. Now open the ActionScript file, and continue reading below

__NOTE:__ When running the SWF if you type `DEBUG T` it will show helpful information to the screen.

## Structure

### Language and Settings

There is **one** ActionScript base file for all the FLAs of all the sizes.

In order to get a different language for the FLA you must set the proper language string in the ActionScript Settings. This is done by  going to 

`File < ActionScript Settings < Config Constants`

And changing `CONFIG::LANGUAGE` to any of the three languages `EN, FR, or BI`. 
If the language does not match it will default to `EN`.
 
There is also another constant set called `CONFIG::SETTINGS`.This constant accepts any **valid** JSON string.
This setting can be used to set many different purposes, for example if you need to handle something for only a specific file. 
For the portrait boards you must add a flag in the `CONFIG::SETTINGS` object.

Here is an example of `CONFIG:SETTINGS`.

```javascript
{
    // Enables portrait mode, which forces the LANGUAGE to EN, and rotates the board
    "portrait": true,

    // Allows you to do debug related tasks, it also prints any exceptions to the stage
    "debug": false,

    // You can add anything you want, and handle it in the AS3 code
    "miscSetting": "I am an extra setting"
}
```

In the AS3 code the `CONFIG::LANGUAGE` and `CONFIG::SETTINGS` can be accessed as `_boardSettings`.

## Structure

### Movie Clips

There is a `Template_EN` and `Template_FR` MovieClip available for easy editing.  You just need to update the image placeholder with the proper asset, the MovieClip has existing layers for the background and the reference.

The FLA should have the following Movie Clip structure.

**NOTE:** If a promo needs calories, then use the following naming convention `Template_Cal_FR`

```text
-Promos
 |-- Default Promo
 | |--- EN
 | | |--- CoffeeQuality_EN <-- MovieClip
 | | |--- Asset_EN.jpg
 | |--- FR
 |   |--- CoffeeQuality_FR <-- MovieClip
 |   |--- Asset_FR.jpg
 |-- Template
 | |--- EN
 | | |--- __REF_[Size]_PromoReference_EN.jpg
 | | |--- _[Size]_PromoImage_EN.jpg
 | | |--- Template_EN <-- MovieClip
 | |--- FR
 |   |--- __REF_[Size]_PromoReference_FR.jpg
 |   |--- _[Size]_PromoImage_FR.jpg
 |   |--- Template_FR <-- MovieClip
```

_**NOTE:**_ The promo MovieClips **need** to be of class `com.ek3.dmb.display.AnimatingObject` or `com.ek3.dmb.display.DynamicPromo`

The promo playlist is dynamically created based on the current language of the board.  As an example if the board language is set to `EN` then the code will look for `PromoName_EN`, and if the language is set to `BI` it will look for `PromoName_EN` and `PromoName_FR`.  So **ensure** that your promo MovieClips are named correctly, with the language tag added on.

_NOTE:_ Since the playlist is dynamically created, there will be **no** compile error if a linkage does not exist, only a run-time error will be logged, and default will play instead.  So make sure you label them correctly.

### ActionScript

The provided `TDLCAN_Template.as` is ready to handle and build the promo as long as the structure above is followed.
Inside the AS3 file you will find a comment informing you were you need to make changes to the file.

Look for the function `determineActivePromo()` this is where you will check for the existence of your nodes, or any other additional logic.  
Make sure to return an array of `DynamicPromo` or `AnimatingObject`, if `null` is returned than the default promo will play instead.

**NOTE:** If calories are needed for a promo, ensure the `PromoName_Cal_EN` MovieClip exists, and when calling `createPromoInstances()` pass `{ calories: true }` as the third argument.  This will tell the function to grab the prices __and__ the calories from the `MenuboardFeedItem`.

Here is an example of the boilerplate code that needs to be changed:

```javascript
private function determineActivePromo():Array {
    /** Customize promo here - Ensure promo linkages are suffexed with language tag ex: Promo1_EN, Promo2_FR **/

    // Promo - Main
    const mainPromo:String = "Template"; // Tries to instanciate mainPromo + _ + Language
    const replace2:MenuboardFeedItem = getItem("replace_node2");
    const replace3:MenuboardFeedItem = getItem("replace_node3");

    // multipleAreAvailable(array, string):Boolean takes an array of MenuboardFeedItems or serverNames as a String.
    // It will return true if all of the items in the array are available in the XML
    const replaceIsAvailable:Boolean = multipleAreAvailable(["replace_with_promo_node", replace2, replace3], mainPromo);
    if (replaceIsAvailable) {
        // Pass the MenuboardFeedItems to createPromoInstances(), to enable calories pass { calories: true } as 3rd param
        return createPromoInstances(mainPromo, [replace2, replace3]);
    }

    // Add addtional promos/checks here

    /** End customization **/
    return null; // doBuild() will use DEFAULT_PROMO_NAME
}
```

## Publishing

Publishing can be done by changing the `CONFIG::LANGUAGE` to your desired language, export, then rename the SWF. Or the recommended way is to use `publish.jsfl` from the template directory.

The script will handle all of the naming and exporting of the files.

**NOTE:** If you run the script once, then it will copy itself to your commands folder, so you can just click `Commands > run_publish`

Script steps

1. File selection window
    - Scans the current active directory
    - Lists all of the FLA's it finds
    - Can change the directory to find different promos
1. Choose which files you wish to publish
1. Options Window
    1. Choose the language you wish to publish
    1. Force portrait mode
        - This will treat **every** file as a portrait file, which will rotate the promo
    1. Enable debug mode
        - Useful when testing the file, display helpful info
    1. Other settings
        - Add custom settings to be passed to each promo **NOTE** must be a valid JSON string
1. Hit okay
1. Grab a coffee, this might take awhile
1. Each file will be opened, published with language(s) then closed
1. SWFs and log file will be saved to `./SWF` or the user specified folder
