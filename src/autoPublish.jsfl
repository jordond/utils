﻿var version = '2.0.3';
var author = 'Jordon de Hoog';

var FLfile = FLfile;
var fl = fl;

// Clear output panel, and copy script into the command folder
fl.outputPanel.clear();
var scriptName = fl.scriptURI.substr(fl.scriptURI.lastIndexOf('/') + 1);
FLfile.write(fl.configURI + 'commands/run_' + scriptName, 'fl.runScript("' + fl.scriptURI + '");');

/**
 * TODO 
 * 
 * add .promorc file functionality.  Read settings from the directory
 * 
 * Modify the additional settings field to be comma seperated or new line seperated
 * 
 * read past config from a temp file
 * 
 * Add field in promoOptions UI for setting the library path
 * 
 * Create seperate script that will copy the Templates, and rename them and the AS3 code
 */

// Store messages for post publish report
var logs = [];
function log(message) {
    if (message) {
        logs.push(formatConsoleDate() + message);
        fl.trace(formatConsoleDate() + message);
    }
}
function printLog(reportFilename) {
    var report = logs.join('\n');
    fl.outputPanel.clear();
    fl.trace(report);
    if (reportFilename) {
        reportFilename +=  '/autoPublisher.log';
        log('Saving log -> ' + FLfile.uriToPlatformPath(reportFilename));
        FLfile.write(reportFilename, report, 'append');
    }
}

var publish_profile_name = '@temp';
var publish_folder_suffix = '/SWF';
var publish_folder = '';
var publish_initial_version = '(v1)';

// Constants
var monthMap = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
var language_all = 'All';
var language_en = 'EN';
var language_fr = 'FR';
var language_bi = 'BI';
var board_none = 'NONE';
var board_lunch = 'LUNCH';
var board_hot = 'HOT';
var board_cold = 'COLD';

var isLTO = false;

var log_output = '';

var currentDOM = fl.getDocumentDOM();
var currentDOMPath = '';

if (!currentDOM) {
    log('No source FLA file is open')
    confirm('No file is open, would you like to open one now?');
    if (confirm) {
        var file = fl.browseForFileURL('open', 'Which file would you like to open?');
        if (file) {
            currentDOM = fl.openDocument(file);
        }
    }
}

// Script info
var scriptStartDate = new Date();
log([
    '',
    'AutoPublisher - Promo and LTO auto publishing script',
    'Version: v' + version,
    'Author : ' + author,
    'Date   : ' + scriptStartDate.toDateString() + ' ' + scriptStartDate.toLocaleTimeString()
].join('\n'));

if (currentDOM) {
    var currentDirectory = FLfile.platformPathToURI(currentDOM.path.replace(currentDOM.name, ''));
    currentDOMPath = FLfile.platformPathToURI(currentDOM.path);
    log('Current FLA -> ' + currentDOM.name);
    init(currentDirectory);
} else {
    log('No valid FLA was found or chosen');
}

function init(directory) {
    log('Initial directory -> ' + FLfile.uriToPlatformPath(directory));
    log('Initializing Script');
	
    var publishCount = search(directory);

    if (publishCount) {
        var successes = publishCount.filter(function (x) { return x; }).length;
        var errors = publishCount.length - successes;

        log(successes + ' File(s) Published');
        log(errors + ' File(s) Failed');
        if (errors > 0) {
            log('There were some errors publishing, check the logs');
        }
        log('Script will now exit!');
        printLog(publish_folder || log_output);
    } else {
        log('Script has finished without publishing');
    }
}

function search(directory) {
    // Get all FLA's in the given directory
    var searchResults = findAllFLAsInDirectoryAndSubFolders(directory);
    if (searchResults.length === 0) {
        return searchNewDirectoryOrQuit();
    }

    // Create and display the dialog to display flas
    var selectionResult = displayAndGetSelectedFiles(directory, searchResults);

    // If the user wishes to change directory, call search() with the new directory
    if (selectionResult) {
        if (selectionResult.changeDirectory) {
            log('User has opted to search a new directory');
            return search(selectionResult.changeDirectory);
        } else {
            var selectedFLAs = filterSelectionResults(selectionResult);
            if (selectedFLAs.length === 0) {
                var tryAgain = confirm('No source files were selected, do you want to try again?');
                if (tryAgain) {
                    return search(directory);
                }
                log('No source files were selected');
                return false;
            }
            log(selectedFLAs.length + ' File(s) have been selected');
            log_output = directory + publish_folder_suffix + '/';

            // Show Promo Options dialog
            isLTO = selectedFLAs.some(function (x) { return x.indexOf('_LTO_') !== -1; })
            var promoOptions = displayAndGetPromoOptions(selectedFLAs.length, directory, isLTO);
            if (promoOptions) {
                if (promoOptions.action === 'back') {
                    return search(directory);
                }
                promoOptions.isLTO = isLTO;

                // Save all open documents
                // fl.saveAll()

                // Build a map of each selected file with their corresponding folder.
                var selectedFLAMap = {};
                searchResults.forEach(function (result) {
                    for (var i = 0; i < result.files.length; i++) {
                        var index = selectedFLAs.indexOf(result.files[i]);
                        if (index !== -1) {
                            selectedFLAMap[result.folder] = [].concat(selectedFLAMap[result.folder] || [], [result.files[i]]);
                        }
                    }
                });

                // Publish each file
                // log('Closing current File in preperation to publish');
                currentDOM.save();
                // currentDOM.close();
                return publishFiles(selectedFLAMap, promoOptions);
            }
        }
    }

    // If we made it here, something didn't go right
    return false;
}

/** Folder and File selection methods **/

function findAllFLAsInDirectory(directory) {
    var list = FLfile.listFolder(directory + '/*.fla', 'files').filter(function (item) { return item.indexOf('RECOVER') === -1 });
    log('Searching -> ' + FLfile.uriToPlatformPath(directory));
    log('  -> Found ' + list.length + ' FLA\'s');
    return { folder: directory, files: list };
}

// TODO add real recursiveness to get even more subdirs
function findAllFLAsInDirectoryAndSubFolders(startingDirectory) {
    var platformPath = FLfile.uriToPlatformPath(startingDirectory);
    var combinePaths = function (x) { return FLfile.platformPathToURI(platformPath + '\\' + x); };
    var directories = FLfile.listFolder(startingDirectory, 'directories').map(combinePaths);
    directories.unshift(startingDirectory);
    log('Searching ' + directories.length + ' folders inside -> ' + platformPath);
    return directories
        .map(function (directory) { return findAllFLAsInDirectory(directory); })
        .filter(function (x) { return x.files.length !== 0 });
}

function displayAndGetSelectedFiles(rootDir, folderFileList) {
    return showDialogForResult('TEMP_promoSelect.xml', createFileSelectionDialog(rootDir, folderFileList));
}

function displayAndGetPromoOptions(selectedFileCount, currentDirectory, containsLtoFiles) {
    var result = showDialogForResult('TEMP_promoOptions.xml', createPromoOptionsDialog(selectedFileCount, currentDirectory, containsLtoFiles));
    if (result) {
        if (result.action === 'back') {
            return result;
        }
        // Convert language to an array of languages
        result.language = result.language === language_all ? [language_en, language_fr, language_bi] : [result.language];
        result.forcePortrait = result.forcePortrait === 'true';
        result.debugMode = result.debugMode === 'true';
        result.overwrite = result.overwrite === 'true';
        result.archive = result.archive === 'true';
        result.stripUnused = result.stripUnused === 'true';

        var tempDir = FLfile.platformPathToURI(result.publishDir);
        if (FLfile.exists(tempDir)) {
            publish_folder = tempDir;
        } else {
            publish_folder = currentDirectory + publish_folder_suffix;
        }

        result.settings = parseSettingsJSON(result.settings);
        if (!result.settings) {
            alert('Extra settings is not a valid JSON string again, correct it now or delete entry');
            return displayAndGetPromoOptions(selectedFileCount, currentDirectory, containsLtoFiles);
        }
        result.settings.debug = result.debugMode;

        log('Chosen Promo Options');
        log('   -> Publish folder: ' + FLfile.uriToPlatformPath(publish_folder));
        log('   -> Override files: ' + result.overwrite);
        log('   -> Archive files: ' + result.archive);
        log('   -> Language: ' + result.language);
        log('   -> Ignore Unused: ' + result.stripUnused);
        log('   -> Force Portrait Mode: ' + (result.forcePortrait ? 'Enabled' : 'Disabled'));
        log('   -> Debug Mode: ' + (result.debug ? 'Enabled' : 'Disabled'));
        log('   -> Misc Settings: ' + stringify(result.settings));
        if (containsLtoFiles) {
            log('Detected as an LTO file');
            log('   -> LTO Board Type: ' + result.board);
        }
        
        result.settings.tokenizeParam = result.tokenizeParam === 'true';
    }
    return result;
}

function showDialogForResult(xmlName, xmlData) {
    var dialogPath = fl.configURI + '/' + xmlName;
    FLfile.write(dialogPath, xmlData);
    var result = currentDOM.xmlPanel(dialogPath);
    return result.dismiss === 'accept' ? result : false;
}

function searchNewDirectoryOrQuit() {
    var searchNewFolder = confirm('No source FLA\'s were found, would you like to search another folder?');
    if (searchNewFolder) {
        var newFolder = fl.browseForFolderURL("Select the folder containing the FLA\'s you wish to publish");
        if (newFolder) {
            return search(newFolder);
        } else {
            return searchNewDirectoryOrQuit();
        }
    }
    return false;
}

/**
 * Filter the results of the xmlui object to find all of the checkboxes that were marked as checked
 * 
 * @param {Object} results Result object that contains keys of the xmlui components and their values
 * @returns Array   Filenames of the files that were selected
 */
function filterSelectionResults(results) {
    var selected = [];
    for (var key in results) {
        if (!results.hasOwnProperty(key) || key.indexOf('cb_') === -1) {
            continue;
        } else if (results[key] === 'true') {
            selected.push(key.replace('cb_', ''));
        }
    }
    return selected;
}

/** File publishing **/

/**
 * Publish all the files in the file map
 * 
 * fileMap: { folder1: [file1, file2, file3], folder2: [file1] }
 * options: { language[], forcePortrait:Boolean, settings:String - JSON String }
 * 
 * @param {Object} fileMap Object with directory as a key, and array of files as the value
 * @param {Object} options Additional options for promos
 */
function publishFiles(fileMap, options) {
    var openDocuments = fl.documents;
    var publishedFileCount = [];
    for (var folder in fileMap) {
        if (!fileMap.hasOwnProperty(folder)) continue;

        log('Publishing ' + fileMap[folder].length + ' files from -> ' + FLfile.uriToPlatformPath(folder));
        
        // Check if output folder exists, create it if it doesn't
        var publishFolderCreated = FLfile.createFolder(publish_folder);
        if (publishFolderCreated) {
            log('   -> Created publish folder -> "' + FLfile.uriToPlatformPath(publish_folder) + '"');
        } else {
            log('   -> Using existing publish folder');
        }
        
        fileMap[folder].forEach(function (filename) {
            // Check if promo is portrait, if so enable portrait and only publish EN
            var languages = options.language;
            var settings = options.settings;
            var isPortrait = fileIsPortrait(filename);
            if (isPortrait || options.forcePortrait) {
                languages = [language_en];
                settings.portrait = true;
            } else {
                settings.portrait = false;
            }
            if (options.board) {
                settings.board = options.board;
            }

            var currentFilesInPublishfolder = FLfile.listFolder(publish_folder, 'files');

            // Get and open the selected file
            try {
                var currentFile = fl.openDocument(folder + '/' + filename);
                log('   -> Opening -> ' + filename);
                
                var publishFilename = filename.replace('.fla', '');
                if (settings.tokenizeParam) {
                    publishFilename = tokenizeParameters(publishFilename, settings);
                }

                // Get a list of the DynamicPromos
                var listOfDynamicPromos = findDynamicPromosInLibrary(currentFile);
                var listOfLTOGenericsToIgnore = listOfDynamicPromos.filter(function (x) { 
                    if (x.linkageClassName.indexOf('Generic') !== -1) {
                        return x.linkageClassName.indexOf(settings.board) === -1;
                    }
                    return false;
                });

                // Loop Language array to publish file in each language
                var published = languages.map(function (language) {
                    log('     -> Start ' + language);
                    if (options.stripUnused) { // TODO BROKEN
                        var itemsToIgnore = getItemsToIgnoreFromLanguage(listOfDynamicPromos, language);
                        if (options.isLTO) {
                            itemsToIgnore = removeDuplicates(itemsToIgnore.concat(listOfLTOGenericsToIgnore), 'linkageClassName');
                        }
                        var ignoreItemsString = itemsToIgnore.map(function (x) { return x.linkageClassName; }).join(', ');
                        log(ignoreItemsString ? '           -> Ignored: ' + ignoreItemsString : "");
                        toggleLibraryItemsStatus(itemsToIgnore);
                    }
                    var published = publishFile(currentFile, language, folder, publishFilename, settings, currentFilesInPublishfolder, options);
                    if (options.stripUnused) {
                        toggleLibraryItemsStatus(itemsToIgnore);

                    }
                    log('     -> Finish ' + language);
                    return published;
                });

                publishedFileCount = publishedFileCount.concat(published);
                if (currentFile.path !== currentDOM.path && !documentIsAlreadyOpen(openDocuments, currentFile)) {
                    log('   -> Closing -> ' + filename);
                    currentFile.save();
                    currentFile.close();
                }
            } catch (ex) {
                log('       -> ' + ex);
                log('       -> ERROR: Publishing file')
                log('       -> ' + FLfile.uriToPlatformPath(folder + '/' + filename));
                publishedFileCount.push(false);
            }
        });
    }
    return publishedFileCount;
}

function removeDuplicates(arr, property) {
    var lookup = {};
    var deduplicated = [];

    for (var i in arr) {
        lookup[arr[i][property]] = arr[i];
    }

    for (var j in lookup) {
        deduplicated.push(lookup[j]);
    }
    return deduplicated;
}

function toggleLibraryItemsStatus(items) {
    items.forEach(function (item) {
        if (item.linkageExportForAS === true) {
            item.linkageExportForAS = false;
        } else {
            item.linkageExportForAS = true;
            item.linkageExportInFirstFrame = true;
        }
    });
}

function getItemsToIgnoreFromLanguage(items, currentLanguage) {
    var languageToIgnore = "";
    if (currentLanguage !== language_bi) {
        languageToIgnore = currentLanguage === language_en ? language_fr : language_en;
    } else if (currentLanguage === language_bi && isLTO) {
        languageToIgnore = language_fr;
    }
    return items.filter(function (x) { return languageToIgnore && x.linkageClassName.indexOf(languageToIgnore) !== -1; });
}

function findDynamicPromosInLibrary(document) {
    var acceptedClasses = ["AnimatingObject", "DynamicPromo"];
    var items = document.library.items;
    return items.filter(function (item) {
        if (item.itemType === "movie clip") {
            if (item.linkageExportForAS) {
                var baseClass = item.linkageBaseClass.split('.').pop();
                return acceptedClasses.indexOf(baseClass) !== -1;
            }
        }
    });
}

function documentIsAlreadyOpen(openDocuments, target) {
    openDocuments = openDocuments ? openDocuments : [];
    var matched = openDocuments.filter(function (d) { return d.name === target.name; });
    return matched.length > 0;
}

function tokenizeParameters(originalFilename, promoSettings) {
    var ignoreProps = ['debug', 'portrait', 'tokenizeParam', 'board'];
    var tokenizedFilename = '';
    for (var key in promoSettings) {
        if (!promoSettings.hasOwnProperty(key) || ignoreProps.indexOf(key) !== -1) continue;
        if (typeof promoSettings[key] === 'boolean') {
            tokenizedFilename += '_[' + key + ']';
        } else {
            tokenizedFilename += '_[' + key + '-' + promoSettings[key] + ']';
        }
    }
    return originalFilename + tokenizedFilename;
}

function getDateForFilename() {
    var date = new Date();
    var day = date.getDate();
    var month = monthMap[date.getMonth()]
    var year = date.getFullYear() - 2000;

    return [(day < 10 ? '0' + day : day), month, year].join('');
}

function publishFile(document, language, folder, filename, settings, publishedFileList, options) {
    var publishFilename = '';
    if (filename.indexOf('MULTI') !== -1) {
        publishFilename = filename.replace('MULTI', language);
    } else {
        publishFilename = filename + (settings.portrait ? '' : '_' + language);
    }

    if (settings.debug) {
        log('       -> WARNING: Debug mode is enabled DO NOT USE IN PRODUCTION')
        publishFilename += '_' + 'DEBUG'
    }

    if (settings.board) {
        log('       -> LTO board type: ' + settings.board);
        publishFilename += '_' + settings.board;
    }

    if (settings.portrait) {
        log('       -> as PORTRAIT');
    }

    // Check to see if a swf already exists, if so increase the version number
    publishFilename = checkForExisitingSwf(publishedFileList, publishFilename, options);
    log('       -> Saved: ' + publishFilename + '.swf');

    // publish the file
    if (document.publishProfiles.indexOf(publish_profile_name) != -1) {
        document.currentPublishProfile = publish_profile_name;
        document.deletePublishProfile();
    }
    document.addNewPublishProfile(publish_profile_name);
    document.currentPublishProfile = publish_profile_name;

    var fullPublishName = publish_folder + '/' + publishFilename + '.swf';
    document.importPublishProfileString(generatePublishProfile(document, fullPublishName, language, settings));

    document.exportSWF(fullPublishName, true);
    document.deletePublishProfile();

    // if (!fl.fileExists(fullPublishName)) {
    //     log('   -> ERROR: File was not found where it was supposed to be, something went wrong');
    //     return false;
    // }
    return true;
}

function checkForExisitingSwf(filelist, filename, options) {
    var regExp = /(?:\(v(\d+)\))/;
    var existingSWFs = filelist
        .filter(function (x) { return x.indexOf(filename) !== -1; })
        .filter(function (x) { return x.match(regExp); })
        .map(function (x) { return x.replace('.swf', ''); });
    
    if (existingSWFs.length > 0) {
        var newestVersion = existingSWFs[existingSWFs.length - 1];
        return newestVersion.replace(regExp, function (str, p1) {
            var version = '(v' + (Number(p1) + (options.overwrite ? 0 : 1)) + ')';
            var message = options.overwrite ? 'Overwriting existing version' : 'Found existing file, bumping to version';
            log('           -> ' + message + ' -> ' + version);
            
            if (options.archive && !options.overwrite) {
                var versionFolder = publish_folder + '/_Archive/v' + p1;
                var created = FLfile.createFolder(versionFolder);
                if (created) {
                    log('           -> Created archive folder -> ' +  FLfile.uriToPlatformPath(versionFolder));
                }

                log('           -> Archiving ' + str);
                var archiveFilename = '/' + newestVersion + '.swf';
                var copied = FLfile.copy(publish_folder + archiveFilename, versionFolder + archiveFilename);
                if (copied) {
                    var deleted = FLfile.remove(publish_folder + archiveFilename);
                    if (!deleted) {
                        log('           -> WARNING: File was archived, but original could not be deleted');
                    }
                } else {
                    log('           -> WARNING: Was not archived, file already exists');
                }
            }
            return version;
        });
    } else {
        filename += '_' + getDateForFilename();
    }
    return filename + '_' + publish_initial_version;
}

function generatePublishProfile(currentDoc, publishPathandFilename, language, settings) {
    // Stringify the settings object, then replace all double quotes with a token to be replaced by AS3 file ($)
    var swfConstantsXML = new XML([
        '<AS3ConfigConst>',
            'CONFIG::LANGUAGE="' + language + '";',
            'CONFIG::SETTINGS="' + stringify(settings).replace(/(["])(["])?/g, '$') + '";',
            (settings.board ? 'CONFIG::BOARD="' + settings.board + '";' : ''), 
        '</AS3ConfigConst>',
    ].join(''));

    var publishProfile = new XML(currentDoc.exportPublishProfileString('Default'));
    publishProfile.@name = publish_profile_name;
    publishProfile.PublishFormatProperties.flashFileName = publishPathandFilename;
    publishProfile.PublishFormatProperties.html = 0;
    publishProfile.PublishFlashProperties.AS3ConfigConst = swfConstantsXML;
    return publishProfile;
}

/** UI Related Methods **/

function createFileSelectionDialog(rootDirectory, items) {
    if (items.constructor !== Array) {
        items = [items];
    }

    // Get all the file names for creating the selectAll and selectNone functions
    var flattenedFileList = items.map(function (item) { return item.files; });
    flattenedFileList = [].concat.apply([], flattenedFileList);

    // Split the array into chunks based on how many folders were scanned
    var chunkLength = items.length >= 7 ? 3 : 2;
    var itemChunks = chunkifyList(items, chunkLength);

    var s = "";
    s += '<dialog id="selectSource" title="Batch Promo Publisher" buttons="accept, cancel">';
    s +=    '<script>';
    s +=        'function selectAll() {';
    s +=            flattenedFileList.map(function (item) { return 'fl.xmlui.set("cb_' + item + '", true);' }).join('');
    s +=        '}';
    s +=        'function selectNone() {';
    s +=            flattenedFileList.map(function (item) { return 'fl.xmlui.set("cb_' + item + '", false);' }).join('');
    s +=        '}';
    s +=        'function changeDirectory() {';
    s +=            'var newFolder = fl.browseForFolderURL("Select the folder containing the FLA\'s you wish to publish");'
    s +=            'if (newFolder) {';
    s +=                'fl.xmlui.set("changeDirectory", newFolder);';
    s +=                'fl.xmlui.accept();';
    s +=            '}';
    s +=        '}';
    s +=        'function hideOnCreate(id) {';
    s +=            'fl.xmlui.setVisible(id, false);';
    s +=        '}';
    s +=    '</script>';
    s +=    '<vbox style="overflow:auto">';
    s +=        '<hbox>'
    s +=            '<label width="300" value="Select the promo files to publish" />';
    s +=            '<textbox width="10" id="changeDirectory" value="" oncreate="hideOnCreate(\'changeDirectory\')" />';
    s +=        '</hbox>'
    s +=        '<label value="Root Folder: ' + FLfile.uriToPlatformPath(rootDirectory) + '" /><separator />';
    s +=        '<grid>';
    s +=            '<columns>';
                        for (var j = 0; j < chunkLength; j++) s += '<column />';
    s +=            '</columns>';
    s +=            '<rows>';
    s +=                itemChunks.map(function (row) { return '<row>' + generateFileList(row) + '</row>'; }).join('');
    s +=            '</rows>';
    s +=        '</grid><separator/>';
    s +=        '<hbox>';
    s +=            '<button label="Select All" width="150px" oncommand="selectAll()" />';
    s +=            '<button label="Select None" width="150px" oncommand="selectNone()" />';
    s +=            '<button label="Change Directory" width="60px" oncommand="changeDirectory()" />';
    s +=        '</hbox><separator/>';
    s +=    '</vbox>';
    s += '</dialog>';
    return s;

    function chunkifyList(list, lengthOfChunk) {
        return list.map(function (item, index) {
            return index % lengthOfChunk === 0 ? list.slice(index, index + lengthOfChunk) : null;
        }).filter(function (item) { return item; });
    }

    function generateFileList(filesList) {
        return filesList.map(function (item, index) {
            var folderName = './' + FLfile.uriToPlatformPath(item.folder).split('\\').pop();
            var sub = "";
            sub += "<vbox>";
            sub +=    '<label value="' + folderName + '" />';
            sub +=    item.files.map(function (file) {
                          var isCurrentFile = file === currentDOM.name;
                          return '<checkbox id="cb_' + file + '" label="' + file + '" checked="' + isCurrentFile + '" />';
                      }).join('');
            sub += "</vbox>";
            return sub;
        }).join('');
    }
}

function createPromoOptionsDialog(selectedFilesCount, currentDirectory, containsLtoFiles) {
    var showLtoOptions = containsLtoFiles;
    var ltoOptions = [
        '<label value="LTO Specific Options" />',
        '<radiogroup id="board" tabindex="5">',
            '<radio label="' + board_none + '" accesskey="n" selected="true" />',
            '<radio label="' + board_lunch + '" accesskey="l" />',
            '<radio label="' + board_hot + '" accesskey="h" />',
            '<radio label="' + board_cold + '" accesskey="c" />',
        '</radiogroup>',
        '<spacer />'
    ].join('');

    return [
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<dialog id="promoOptions" title="Choose Promo Options" buttons="accept, cancel">',
            '<script>',
                'function notify(id, message) {',
                    'if (fl.xmlui.get(id) === "true")',
                    'alert("WARNING: " + message)',
                '}',
                'function hideOnCreate(id) {',
                    'fl.xmlui.setVisible(id, false);',
                '}',
                'function goBack() {',
                    'fl.xmlui.set("action", "back");',
                    'fl.xmlui.accept();',
                '}',
                'function folderSelect(id) {',
                    'var dir = fl.browseForFolderURL("Choose a folder");',
                    'fl.xmlui.set(id, FLfile.uriToPlatformPath(dir) || fl.xmlui.get(id));',
                '}',
            '</script>',
            '<vbox>',
                '<label value="Selected Promos: ' + selectedFilesCount + '" />',
                '<label value="Choose publish folder" />',
                '<textbox id="publishDir" multiline="false" width="300" value="' + FLfile.uriToPlatformPath(currentDirectory) + publish_folder_suffix + '" />',
                '<button label="Browse" width="30" oncommand="folderSelect(\'publishDir\')" />',
                '<checkbox id="overwrite" label="Overwrite existing SWFs" onchange="notify(\'overwrite\', \'This will disable the file version checking and override any existing published SWFs\')" />',
                '<checkbox id="archive" label="Archive the old versions" checked="true" />',
                '<separator />',
                '<hbox>',
                    '<label value="Choose which language to publish:" />',
                    '<textbox width="10" id="action" value="" oncreate="hideOnCreate(\'action\')" />',
                '</hbox>',
                '<radiogroup id="language" tabindex="5">',
                    '<radio label="' + language_all + '" accesskey="a" selected="true"/>',
                    '<radio label="' + language_en + '" accesskey="e" />',
                    '<radio label="' + language_fr + '" accesskey="f" />',
                    '<radio label="' + language_bi + '" accesskey="b" />',
                '</radiogroup>',
                '<checkbox id="stripUnused" label="Ignore unused language MovieClips (reduces size)" checked="true" />',
                '<spacer />',
                (showLtoOptions ? ltoOptions : ''),
                '<checkbox id="forcePortrait" label="Force Portrait Mode" onchange="notify(\'forcePortrait\', \'By default this script will auto detect portrait mode based on the filename of the file, with this option enabled ALL files will be treated as portrait mode\')" />',
                '<checkbox id="debugMode" label="Enable Debug Mode" onchange="notify(\'debugMode\', \'Enabling this for production is a bad idea, this will enable many debug features.\')" />',
                '<spacer />',
                '<label value="Other Settings: (Note: Must be a valid JSON string)" />',
                '<textbox id="settings" multiline="true" value="{}" tabindex="1" size="12" literal="false" height="100" width="300" />',
                '<checkbox id="tokenizeParam" label="Add additional settings to the filename" />',
                '<button label="Back to File Selection" width="190" oncommand="goBack()" />',
                '<separator />',
            '</vbox>',
        '</dialog>'
    ].join('');
}

/** Utility Methods */

function fileIsPortrait(filename) {
    var portraitTokens = ['Portrait', 'portrait', 'prt', 'PRT', '1Third', '1third', 'PORT', 'port'];
    var filtered = portraitTokens.filter(function (token) { return filename.indexOf(token) !== -1 });
    return filtered.length > 0;
}

function stringify(value) {
    var toString = Object.prototype.toString;
    var isArray = Array.isArray || function (a) { return toString.call(a) === '[object Array]'; };
    var escMap = {'"': '\\"', '\\': '\\\\', '\b': '\\b', '\f': '\\f', '\n': '\\n', '\r': '\\r', '\t': '\\t'};
    var escFunc = function (m) { return escMap[m] || '\\u' + (m.charCodeAt(0) + 0x10000).toString(16).substr(1); };
    var escRE = /[\\"\u0000-\u001F\u2028\u2029]/g;
    if (value == null) {
        return 'null';
    } else if (typeof value === 'number') {
        return isFinite(value) ? value.toString() : 'null';
    } else if (typeof value === 'boolean') {
        return value.toString();
    } else if (typeof value === 'object') {
        if (typeof value.toJSON === 'function') { return stringify(value.toJSON()); }
        else if (isArray(value)) {
            var res = '[';
            for (var i = 0; i < value.length; i++)
                res += (i ? ', ' : '') + stringify(value[i]);
            return res + ']';
        } else if (toString.call(value) === '[object Object]') {
            var tmp = [];
            for (var k in value) {
                if (value.hasOwnProperty(k)) tmp.push(stringify(k) + ': ' + stringify(value[k]));
            }
            return '{' + tmp.join(', ') + '}';
        }
    }
    return '"' + value.toString().replace(escRE, escFunc) + '"';
}

function parseSettingsJSON(json) {
    if (json === '' || json === '{}') return {};
    try {
        return eval('(' + json + ')');
    } catch (ex) {
        log('Settings was not a valid JSON string');
        return false;
    }
}

function formatConsoleDate() {
    return '[' + formatDate(':', false, true) + '] ';
}

function formatDate(separator, showDay, showSeconds) {
    var date = new Date();
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var milliSeconds = date.getMilliseconds();
    return [
        (showDay ? (day < 10) ? '0' + day : day + monthMap[date.getMonth()] + separator : ''),
        ((hour < 10) ? '0' + hour: hour) + separator,
        ((minutes < 10) ? '0' + minutes: minutes),
        (showSeconds ? separator + ((seconds < 10) ? '0' + seconds : seconds) : ''),
        (showSeconds ? separator + (Math.floor(milliSeconds / 10)) : '')
    ].join('');
}
